#!/usr/bin/perl 

# Copyright 2013 Catalyst IT Ltd.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Robin Sheat <robin@catalyst.net.nz> 

# This loads the CSV file from liberty that contains the lending data and
# inserts it into the Koha database.

# Options:
#  --lends / -l: the filename of the current lending data
#  --history / -h: the filename of the historical lending data
#  --delete / -d: delete the existing circ data

use strict;
use warnings;

use csvutils;

use C4::Context;
use Getopt::Long;
use Text::CSV_XS;
use Data::Dumper;

# STDC specific - liberty doesn't track what branch issued what, so this is
# where it puts them. Enhancement: take this from the items table.
my $default_branch = 'S';

my ($lends_file, $history_file, $delete);
my ($fieldsep, $quote);

GetOptions(
    'lends|l=s'   => \$lends_file,
    'history|h=s' => \$history_file,
    'delete|d'    => \$delete,
    'fieldsep=s'  => \$fieldsep,
    'quotechar=s' => \$quote,
);

my $fieldsep_char = csvutils::make_csv_char($fieldsep, ',');
my $quote_char = csvutils::make_csv_char($quote, '"');

my $dbh = C4::Context->dbh;
$dbh->{AutoCommit} = 0;
if ($lends_file) {
    print "Processing lends file ($lends_file).\n";
    process_lends($lends_file, 'issues');
    set_item_statuses();
}
if ($history_file) {
    print "\nProcessing history file ($history_file).\n";
    process_lends($history_file, 'old_issues');
}
$dbh->commit();

my $prep_borr;
my %borrowernumber_cache;
sub get_borrower_number {
    my ($num) = @_;

    # Sometimes this value starts with '_' and I don't know why.
    # $num =~ s/^_//;
    return $borrowernumber_cache{$num} if (exists($borrowernumber_cache{$num}));
    $prep_borr = $prep_borr || $dbh->prepare('SELECT borrowernumber FROM borrowers WHERE cardnumber=?');
    my $res = $prep_borr->execute($num);
    die "Database error" if (!defined($res));
    my $row = $prep_borr->fetchrow_arrayref;
    if (!$row) {
        warn "Couldn't find a user for cardnumber $num\n";
        $borrowernumber_cache{$num} = undef;
        return undef;
    }
    my $val = $row->[0];
    $borrowernumber_cache{$num} = $val;
    return $val;
}

sub process_lends {
	my ($file, $table) = @_;
    # Fields
    my $BARCODE = 1;
    my $USER = 3;
    my $DATELENT = 4;
    my $DATEDUE = 5;
    my $DATERETURNED = 6;
    my $NOTES = 8;
    my $csv = get_csv_obj();
    open my $csvfile, '<', $file
        or die "Unable to open $file $!\n";
    my $count = 0;
    my $added = 0;
    $dbh->prepare("DELETE FROM $table")->execute if $delete;
    my $qry = "INSERT INTO $table (borrowernumber, itemnumber, date_due, issuedate, returndate, branchcode, issuingbranch, renewals) VALUES (?,?,?,?,?,?,?,?)";
    my $sth = $dbh->prepare($qry);
    $csv->getline($csvfile); # skip header
    while (my $row = $csv->getline($csvfile)) {
        clean_strings($row);
    	if ($count % 70 == 0) {
        	print "\n".$count." ";
        } else {
        	print ".";
        }
        $count++;
        my $borrowernumber = get_borrower_number($row->[$USER]);
        next if (!defined($borrowernumber));
        my $itemnumber = get_item_number($row->[$BARCODE]);
        next if (!defined($itemnumber));
        my $returndate = $row->[$DATERETURNED] || undef;
        undef($returndate) if $returndate eq '00000000'; # fix liberty null dates
        my $renewals = count_renewals($row->[$NOTES]);
        $added++;
        my $res = $sth->execute($borrowernumber, $itemnumber, $row->[$DATEDUE],
            $row->[$DATELENT], $returndate, $default_branch, $default_branch,
            $renewals);
        if (!defined($res)) {
        	die "Database error\n$borrowernumber, $itemnumber, $row->[$DATEDUE], $row->[$DATELENT], $returndate, $default_branch, $default_branch, $renewals\n".join(', ',@$row)."\n";
        }
    }
    close $csvfile;
    print "\nTotal: $count\tAdded: $added\n";
}

# This sets the loan status of items correctly.
sub set_item_statuses {
    $dbh->prepare('UPDATE items,issues SET items.onloan=issues.issuedate WHERE issues.itemnumber=items.itemnumber')->execute();
}

my $prep_item;
my %item_cache;
sub get_item_number {
    my ($num) = @_;

    # Sometimes this value starts with '_' and I don't know why.
    # $num =~ s/^_//;
    return $item_cache{$num} if exists $item_cache{$num};
    $prep_item = $prep_item || $dbh->prepare('SELECT itemnumber FROM items WHERE barcode=?');
    my $res = $prep_item->execute($num);
    die "Database error" if (!defined($res));
    my $row = $prep_item->fetchrow_arrayref;
    if (!$row) {
        warn "Couldn't find an item for barcode $num\n";
        $item_cache{num} = undef;
        return undef;
    }
    my $val = $row->[0];
    $item_cache{num} = $val;
    return $val;
}

sub count_renewals {
    my ($text) = @_;

    my $count =()= $text =~ /Renewal/g;
    return $count;
}

sub get_csv_obj {
    my $csv = Text::CSV_XS->new({
            binary      => 1,
            eol         => $/,
            allow_loose_quotes  => 1,
            escape_char => '',
            sep_char    => $fieldsep_char,
            quote_char  => $quote_char,
            auto_diag   => 2,
        });
    return $csv;
}

sub clean_strings {
    my ($strings) = @_;

    foreach my $s (@$strings) {
        $s =~ s/\xAE/\n/;
    }
}
