package Elm::ItemMatcher;

# This uses the information that .eLM provides to attempt to match up biblios
# and items.
#
# To use it, populate it with all the items by calling new_item and passing
# in a hashref.
#
# Then later, call attach_items passing in the MARC record and it'll attempt
# to attach the items to it.

use strict;
use warnings;

use Data::Dumper;

sub new {
    my ($class) = shift;
    bless {
        items              => {},
        item_count         => 0,
        dupbarcodes => [],
        @_
    }, $class;
}

sub new_item {
    my ( $self, $item ) = @_;

    if ( $self->{item_count} % 70 == 0 ) {
        print "\n$self->{item_count}  ";
    }
    $self->{item_count}++;

    # We store them first by author, then title, year, then classification.
    # If any are empty, they are explicitly set to the empty string.
    my $classif = $item->{classification};
    my $author  = $item->{author};

    # This is a strange field that contains newline separated information
    # about the record. We're going to use it to match against a biblio record.
    my $recorddetails = $item->{recorddetails};
    my @details_parts =
      split( /[\r\n]+/, $recorddetails );
    s/^\s*(.*?)\s*$/$1/ foreach (@details_parts);

    # Sometimes the newline is missing in the first row
    if ( $details_parts[0] && $details_parts[0] =~ /     \w+,/ ) {
        my ( $l, $r ) = $details_parts[0] =~ /^(.*?)\s*     (.*)$/;
        shift @details_parts;
        unshift @details_parts, $r;
        unshift @details_parts, $l;
    }

    if ( !$classif ) {

        # This means that the details will need to be shuffled a bit
        unshift @details_parts, '';
    }

    # Sometimes there are leading empty records
    shift @details_parts while ( @details_parts && !$details_parts[0] );

    # Work out what parts to keep and what to throw away. Hopefully everything
    # has a classification on the first row, and a title on second or third.
    # Year on the last.
    my ( $title, $year );

    #    print '[ '.join(' || ', @details_parts)."]\n";
    if ( @details_parts == 4 ) {

        # Easy case
        $title = $details_parts[2];
        $year  = $details_parts[3];
    }
    elsif ( @details_parts == 3 ) {

        # Has the author gone, or the year?
        my $last = $details_parts[2];
        if ($author) {

            # the year has gone
            $year  = '';
            $title = $last;
        }
        else {    # the author has gone
            $year  = $last;
            $title = $details_parts[1];
        }
    }
    elsif ( @details_parts == 2 ) {

        # Probably both gone.
        $title = $details_parts[1];
        $year  = '';
    }
    else {
        $self->{missed_item}
          ->( $item, "Not enough information to match to a biblio." );
        print 'm';
        return;
    }
    print '.';

    # 'Year' is sometimes more of an edition statement thing. So we just
    # grab 4 digits out of it if we can.
    $year =~ s/^.*?(\d\d\d\d).*$/$1/g;

    # Sometimes it's d/m/y
    my ($year_part) = $year =~ m|\d+/\d+/(\d+)|;
    if ( defined($year_part) ) {
        if ( $year_part < 12 ) {
            $year = $year_part + 2000;
        }
        elsif ( $year_part < 99 ) {
            $year = $year_part + 1900;
        }
        else {
            $year = $year_part;
        }
    }
    my $key = join( ' -- ', strip( $author, $title, $year, $classif ) );
    die $key if $key =~ /Woolard/;
    push @{ $self->{items}{$key} }, $item;
}

sub attach_items {
    my ( $self, $record ) = @_;

    # Extract the values from the record that we need to build up the key.
    my $title    = $record->subfield( '245', 'a' ) || '';
    my $subtitle = $record->subfield( '245', 'b' ) || '';
    my $author   = $record->subfield( '100', 'a' ) || '';
    my $year     = $record->subfield( '260', 'c' ) || '';
    my $classif  = $record->subfield( '082', 'a' ) || '';
    my $edition  = $record->subfield( '250', 'a' ) || '';

    my $title_f = $title;
    if ($subtitle) {
        $title_f = "$title : $subtitle";
    }
    $year =~ s/^.*?(\d\d\d\d).*$/$1/g;

    # Sometimes it's d/m/y
    my ($year_part) = $year =~ m|\d+/\d+/(\d+)|;
    if ( defined($year_part) ) {
        if ( $year_part < 12 ) {
            $year = $year_part + 2000;
        }
        elsif ( $year_part < 99 ) {
            $year = $year_part + 1900;
        }
        else {
            $year = $year_part;
        }
    }
    my $key = join( ' -- ', strip( $author, $title_f, $year, $classif ) );
    if ( exists $self->{items}{$key} ) {
        my @items = $self->make_952( $self->{items}{$key} );
        delete $self->{items}{$key};
        $record->append_fields(@items);
    }
    else {
        $key = join( ' -- ', strip( $author, $title_f, $edition, $classif ) );
        my @items = $self->make_952( $self->{items}{$key} );
        delete $self->{items}{$key};
        $record->append_fields(@items);
    }
}

sub make_952 {
    my $self  = shift;
    my $items = shift;
    map {
        my $barcode = $_->{barcode};
        if ( $self->{seenbarcodes}{$barcode} ) {
            push @{ $self->{dupbarcodes} }, $barcode;
            $barcode = '';
        }
        else {
            $self->{seenbarcodes}{$barcode} = 1;
        }
        my $f = MARC::Field->new( '952', ' ', ' ', 'p' => $barcode );
        $f->add_subfields(
            'c' => $_->{location},
            '1' => $_->{itemlost},
            'r' => fix_date( $_->{lastseen} ),
            'q' => fix_date( $_->{duedate} ),
            'y' => fix_itemtype( $_->{itemtype} ),
            'a' => fix_branchcode( $_->{currentbranch} )
            ,    # XXX same field for both, verify this
            'b' => fix_branchcode( $_->{currentbranch} ),
            'd' => fix_date( $_->{accessiondate} ),
            'g' => $_->{purchaseprice},
            'v' => $_->{anotherprice},
            'e' => $_->{supplier},
            'h' => $_->{volume},
            'o' => $_->{classification},
            'z' => $_->{notes},
            'w' => fix_date( $_->{purchasedate} ),
        );
        return $f;
    } @$items;
}

sub strip {
    map {
        s/^\s*(.*?)\s*$/$1/;
        $_
    } @_;
}

sub unused_items {
    my ($self) = @_;

    if (wantarray) {
        return map { [ split(/ -- /) ] } keys %{ $self->{items} };
    }
    else {
        return scalar keys %{ $self->{items} };
    }
}

sub duplicate_barcodes {
    my ($self) = @_;

    return @{ $self->{dupbarcodes} };
}

my %months = (
    January   => '01',
    February  => '02',
    March     => '03',
    April     => '04',
    May       => '05',
    June      => '06',
    July      => '07',
    August    => '08',
    September => '09',
    October   => '10',
    November  => '11',
    December  => '12',
);

sub fix_date {
    my ($date) = @_;

    return '' if ( !$date || $date eq '*invalid*' );
    my ( $day, $month_str, $year ) = $date =~ /^(\d\d) (.*) (\d\d\d\d)$/;
    if ( !( $day && $month_str && $year ) ) {
        die "Invalid date found: $date\n";
    }
    my $month = $months{$month_str};
    die "Invalid month: $month_str ($date)\n" if !$month;
    return "$year-$month-$day";
}

my %itemtypes = (
    'Board Book'                            => 'BRDBK',
    'Corporate Library'                     => 'CL',
    '**deleted** Sophistcated Picture Book' => 'SPICBK',
    'Donated or Replaced Fiction'           => 'DONFIC',
    'DVD'                                   => 'DVD',
    'DVD/CD No Charge'                      => 'DVDFREE',
    'Fiction'                               => 'FIC',
    'Fiction - New Zealand'                 => 'FICNZ',
    'Graphic Novels'                        => 'GN',
    'Graphics Junior'                       => 'GJ',
    'Interloan'                             => 'IL',
    'Junior Fiction'                        => 'JFIC',
    'Junior Non-Fiction'                    => 'JNONFIC',
    'Large Print'                           => 'LP',
    'Local Heritage'                        => 'LOCAL',
    'Magazine'                              => 'MAG',
    'Magazine - Junior'                     => 'JMAG',
    'Music CD'                              => 'MUSCD',
    'Non-Fiction'                           => 'NONFIC',
    'Non-Fiction - New Zealand'             => 'NZNONFIC',
    'Paperback'                             => 'PBK',
    'Picture Book'                          => 'PICBK',
    'Picture Book with CD'                  => 'PICBKCD',
    'Playaway Talking Book'                 => 'PLTALKBK',
    'Quick Reads'                           => 'QUICK',
    'Reference'                             => 'REF',
    'Romance Paperback'                     => 'ROMPBK',
    'Sophisticated Picture Book'            => 'SPICBK',
    'Story Books'                           => 'STORYBK',
    'Talking Book National Library'         => 'TALKBKNL',
    'Talking Books'                         => 'TALKBK',
    'Video'                                 => 'VID',
    'Video - No Charge'                     => 'VIDFREE',
    'Young Adult'                           => 'YA',
);

sub fix_itemtype {
    my ($itemtype) = @_;

    return $itemtypes{$itemtype} || 'UNCAT';
}

sub fix_branchcode {
    my ($code) = @_;

    return 'WAIMATE' if ( $code eq 'Waimate' );
    return 'UNKNOWN';
}
1;
