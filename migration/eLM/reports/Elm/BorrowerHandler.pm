package Elm::BorrowerHandler;

use strict;
use warnings;

use base qw(XML::SAX::Base);

use Data::Dumper;

sub start_document {

}

my %ignore_fields = (
    '_200bAmountOwing' => 1,
    '_200hAccountBalance' => 1,
    'numOverdueItems' => 1,
    'objectId' => 1,
    '_200cIssueCount' => 1,
    'dateLastSeen' => 1,
    'hasGuarantees' => 1,
    'hasGuarantor' => 1,
);

my %field_map = (
    '_100aBorrowerNumber'   => 'cardnumber',
    '_100bBorrowerType'     => 'categorycode',
    '_500cSurname'          => 'surname',
    '_500dFirstNames'       => 'firstname',
    '_500aDateOfBirth'      => 'dateofbirth',
    '_500bGender'           => 'sex',
    '_502bEmailAddress'     => 'email',
    '_502cHomeStreetNumber' => 'streetnumber',
    '_502dHomeStreetName'   => 'address',
    '_502fHomeCity'         => 'city',
    '_502gHomePhone'        => 'phone',
    'isDeleted'             => 'isDeleted',
    '_300cCancelled'        => 'cancelled',
    '_300dBlackListed'      => 'debarred',
    'pinNumber'             => 'password',
    '_300aGoneNoAddress'    => 'gonenoaddress',
    '_500oNotesGeneral'     => 'contactnote',
    '_502iGeographicalArea' => 'ward', #  !!! This needs to become an extra attribute
);
sub start_element {
    my ($self, $el) = @_;

    my $name = $el->{Name};
    return if $ignore_fields{$name};
    if ($name =~ /^_/ || $field_map{$name}) {
    	$self->start_field($el);
    	return;
    }
    if ($name eq 'detail') {
    	$self->start_record($el);
    	return;
    }
    return if $name eq 'test-borrowers';
    die "Unknown borrowers element name found: $name\n";
}

sub end_element {
    my ($self, $el) = @_;

    my $name = $el->{Name};
    return if $ignore_fields{$name};
    if ($name =~ /^_/ || $field_map{$name}) {
    	$self->end_field($el);
    	return;
    }
    if ($name eq 'detail') {
    	$self->end_record($el);
    	return;
    }
    return if $name eq 'test-borrowers';
    die "Unknown borrowers element name found: $name\n";
}

sub end_document {

}

sub characters {
    my ($self, $chars) = @_;

    return unless $self->{inside_field};
    $self->{characters} .= $chars->{Data};
}

sub start_record {
    my ($self, $el) = @_;

    $self->{characters} = '';
    $self->{inside_field} = 0;
    $self->{record} = {};
}

sub start_field {
    my ($self, $el) = @_;

    $self->{characters} = '';
    $self->{inside_field} = 1;
}

sub end_field {
	my ($self, $el) = @_;

	my $chars = $self->{characters};
    my $name = $el->{Name};
    my $field = $field_map{$name};
    die "Unknown field encountered: $name\n" if !defined $field;
    return if !$field;
    if ($field eq 'categorycode' && $chars eq '') {
        # There's one of these
        $self->{skip_record} = 1;
        return;
    }
    if ($field eq 'cardnumber' && $chars !~ /^B00/) {
        # Other ones are phantom user objects. This will hopefully be replaced
        # when we get deleted data.
    	$self->{skip_record} = 1;
    	return;
    }
    if ($field eq 'isDeleted' && $chars eq 'true') {
    	$self->{skip_record} = 1;
    	return;
    }
    if ($field eq 'cancelled' && $chars eq 'true') {
    	$self->{skip_record} = 1;
    	return;
    }
    if ($field eq 'gonenoaddress' && $chars eq 'true') {
    	$self->{skip_record} = 1;
    	return;
    }
    $chars = 1 if ($field eq 'debarred' && $chars eq 'true');
    $self->{record}{$field} = $chars;
}

sub end_record {
	my ($self, $el) = @_;

    if ($self->{skip_record}) {
    	$self->{skip_record} = 0;
    	return;
    }
    $self->cleanup($self->{record});
    $self->{borrower_callback}->($self->{record});
}

sub set_borrower_callback {
	my ($self, $sub) = @_;

	$self->{borrower_callback} = $sub;
}

sub cleanup {
	my ($self, $record) = @_;

	$record->{categorycode} = fix_categorycode($record->{categorycode});
	$record->{branchcode} = fix_branchcode($record->{branchcode});
	$record->{dateofbirth} = fix_date($record->{dateofbirth});
}

my %categorymap = (
    'Adult' => 'A',
    'Bulk' => 'B',
    'Child' => 'C',
    'Staff' => 'S',
    'Visitor' => 'V',
);
sub fix_categorycode {
	my ($code) = @_;

    $categorymap{$code};
}

my %months = (
    January   => '01',
    February  => '02',
    March     => '03',
    April     => '04',
    May       => '05',
    June      => '06',
    July      => '07',
    August    => '08',
    September => '09',
    October   => '10',
    November  => '11',
    December  => '12',
);
sub fix_date {
    my ($date) = @_;

    return '' if ( !$date || $date eq '*invalid*' );
    my ( $day, $month_str, $year ) = $date =~ /^(\d\d) (.*) (\d\d\d\d)$/;
    if ( !( $day && $month_str && $year ) ) {
        die "Invalid date found: $date\n";
    }
    my $month = $months{$month_str};
    die "Invalid month: $month_str ($date)\n" if !$month;
    return "$year-$month-$day";
}

sub fix_branchcode {
	return "WAIMATE";
}
