package Elm::BiblioHandler;

# This handles the .eLM XML format.

use strict;
use warnings;

use base qw(XML::SAX::Base);

use Encode qw(encode decode);
use MARC::Record;

use Data::Dumper;

# Fields we want to ignore
my %ignore_field = map { $_ => 1 } qw(
  hasHoldings
  hasItems
  hasOrders
  isActiveAndDeleted
  isSerial
);

# Fields we want to include as part of a record
my %record_fields = map { $_ => 1 } qw(
  isDeleted
  objectId
  isActiveAndCurrent
);

sub start_document {
    my ( $self, $doc ) = @_;

    # We don't really care about the start of a document
}

sub start_element {
    my ( $self, $el ) = @_;
    my $name = $el->{Name};

    return if $ignore_field{$name};

    # These if statements are in reverse order of frequency, for speed.

    # Things that start with _ are fields.
    if ( $name =~ /^_/ || $record_fields{$name} ) {
        $self->start_field($el);
        return;
    }

    # 'detail' is for starting a new record
    if ( $name eq 'detail' ) {
        $self->start_record($el);
        return;
    }

    # 'biblios' is the root element for this, so we ignore it.
    # (also account for typo)
    return if $name eq "biblios" || $name eq "biblos";

    die "Unknown element name found: $name\n";
}

sub end_element {
    my ( $self, $el ) = @_;

    my $name = $el->{Name};
    return if $ignore_field{$name};
    if ( $name =~ /^_/ || $record_fields{$name} ) {
        $self->end_field($el);
        return;
    }
    if ( $name eq 'detail' ) {
        $self->end_record($el);
        return;
    }
    return if $name eq 'biblos';

    die "Unknown closing element name found: $name\n";
}

sub end_document {
    my ( $self, $doc ) = @_;

    # I guess we're done.
}

sub characters {
    my ( $self, $chars ) = @_;
    return unless $self->{inside_field};
    $self->{characters} .= $chars->{Data};
}

# === End of XML::SAX::Base hooks ===

# This indicates that we're at the start of a new MARC record.
sub start_record {
    my ( $self, $el ) = @_;

    $self->{characters} = '';
    $self->{marc}       = MARC::Record->new;
    $self->{marc}->leader('00903pam a2200265 a 4500');
    $self->{inside_field} = 0;
    $self->{skipRecord}   = 0;
}

# We've finished a MARC record.
sub end_record {
    my ( $self, $el ) = @_;
    if ( $self->{skipRecord} ) {
        $self->{skipRecord} = 0;
        return;
    }
    $self->{set_record_callback}->( $self->{marc} );
}

# This starts building a field.
sub start_field {
    my ( $self, $el ) = @_;

    my $name = $el->{Name};
    my ( $tag, $subfield );
    if ( $name =~ /^_/ ) {
        ( $tag, $subfield ) = $name =~ /^_(\d\d\d)(\w)/;
    }
    else {

        # Special things
        if ( $name eq 'objectId' ) {
            ( $tag, $subfield ) = ( '024', 'a' );
        }
        elsif ( $name eq 'isDeleted' ) {
            $self->{checkDeleted} = 1;
        }
        elsif ($name eq 'isActiveAndCurrent') {
        	$self->{checkActiveCurrent} = 1;
        }
    }
    $self->{inside_field}  = 1;
    $self->{characters}    = '';
    $self->{marc_field}    = $tag;
    $self->{marc_subfield} = $subfield;
}

# This saves a field into the MARC record that's being built up.
sub end_field {
    my ( $self, $el ) = @_;

    my $m_field    = $self->{marc_field};
    my $m_subfield = $self->{marc_subfield};
    my $chars      = $self->{characters};
    my $field_name = $el->{Name};

    # Deal with special cases first
    if ( $self->{checkDeleted} ) {
        $self->{checkDeleted} = 0;
        if ( $chars eq 'true' ) {
            $self->{skipRecord} = 1;
            return;
        }
    }
    if ( $self->{checkActiveCurrent} ) {
    	$self->{checkActiveCurrent} = 0;
    	if ($chars ne 'true') {
        	$self->{skipRecord} = 1;
        	return;
        }
    }
    if ($field_name eq 'objectId') {
    	if ($self->{seenObjectIds}{$chars}) {
        	$self->{skipRecord} = 1;
        	return;
        }
        $self->{seenObjectIds}{$chars} = 1;
    }

    return if $chars eq '';
    return if ( $self->{skipRecord} );
    return if !defined($m_field);

    # .eLM puts multiple parts in one field, split up by ' | '
    # we check first, and only apply this if we need speed.
    my @parts;
    if ( $chars =~ / \| / ) {
        @parts = split( / \| /, $chars );
    }
    else {
        @parts = ($chars);
    }
    foreach my $part (@parts) {

        # Skip if empty string
        next if $part =~ /^\s*$/;

        my $field = $self->{marc}->field($m_field);
        if ($field) {
            $field->add_subfields( $m_subfield, $part );
        }
        else {
            $field =
              MARC::Field->new( $m_field, ' ', ' ', $m_subfield => $part );
            $self->{marc}->append_fields($field);
        }
    }
}

# This gets provided a sub that is to be called when a complete biblio record
# has been built up. The sub will be passed the created MARC record.
sub set_record_callback {
    my ( $self, $callback ) = @_;

    $self->{set_record_callback} = $callback;
}

1;
