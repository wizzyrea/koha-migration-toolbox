#!/usr/bin/perl 
#
# Copyright 2010-2013 Catalyst IT Ltd.
#
# This file is useful in conjunction with Koha
#
# Koha is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
#
# Koha is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Koha; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# Written by Robin Sheat <robin@catalyst.net.nz>

=head1 NAME

merge_bib_items.pl - takes a .eLM export and modifies the biblio records to
suit Koha, at the same time merging in the items.

=head1 SYNOPSIS

B<merge_bib_items.pl>
B<--biblios>=I<file>
[B<--items>=I<file>]
[B<--serials>=I<file>]
[B<--serialsbranch>=I<branchcode>]
[B<--transcriptor>=I<transcription code>]
[B<--plugin>=I<file>]
B<--output>=I<file>
[B<--man>]
[B<--help>]

=head1 OPTIONS

=over

=item B<--biblios>=I<file>

This references a file, in USMARC format, that contains the biblio information.
This must have the .eLM standard fields in it, in particular the ID in 998$a.

=item B<--items>=I<file>

This references a file, in tab-separated-value MARC-like format, that contains
the item information.

=item B<--serials>=I<file>

A tab-separated file containing the serials information.

=item B<--serialsbranch>=I<branchcode> (B<--sb>)

A branchcode that is filled in for serial items, as .eLM contains no location
information for them.

=item B<--transcriptor>=I<transcription code>

A code that is put in to indicate the transcribing agency. This is mostly
because 040$c is compulsory in Koha, and this puts something there.

=item B<--plugin>=I<file>

A plugin file is a perl script that can be used to insert code into the
conversion in order to change values on the way through or whatever.
See below for implementation details.

=item B<--output>=I<file>

MARC file is written to this file.

=item B<--man>

This documentation.

=item B<--help>

A usage summary.

=back

=head1 DESCRIPTION

This converts a set of .eLM export files into a form that can work well with
Koha. It modifies the MARC, converts the items tab-separated file and attaches
them as items. Similarly, it creates items from the serial file. If it finds a
record with 998$b as 'SIC', it skips that record. These seem to be recreatable
from the serials file, only there they also are linked to the biblio they
come from.

The MARC changes are:

=over

=item Write the content of 998$a into 014$a (in case it's useful for 
cross-referencing later)

=item 999$c (item type) goes into 942$c

=item Everything in 9xx gets removed (except the 942$c from above.)

=back

=head2 PLUGINS

The plugins are effectively Perl modules that get loaded into the context of
the running script. This module should return a hash containing the keys
listed below and a function reference.

=over

=item B<item>

this gets a hashref of the items (keyed by field name) and it should return
the same (or another) hashref containing anything modified.

=item B<bibtype>

this gets the elm bibtype field and maps it to the item type. This item type
will be the default for items, it may be overwritten later by item-level
item type information. It will also be called to go into the 942$c field.

=item B<serial>

this gets a hashref of the items (keyed by field name) produced by the serials
database. It should return a hashref with any changes in it.

=item B<marc>

this gets the completed MARC record just before it's written out.

=back

=head1 AUTHOR

Robin Sheat <robin@catalyst.net.nz>

=cut

use perl5i::2;

use Encode;
use Encoding::FixLatin qw( fix_latin );
use File::Slurp;
use Getopt::Long;
use MARC::Batch;
use MARC::Charset qw( marc8_to_utf8 );
use Pod::Usage;
use Text::CSV_XS;

use Data::Dumper;

MARC::Charset->ignore_errors(1);
MARC::Charset->assume_unicode(1);

my ( $biblios_file, $items_file, $serials_file, $plugin_file, $output_file );
my ( $serials_branch, $transcriptor );
my ( $man,            $help );

my $result = GetOptions(
    'biblios=s'          => \$biblios_file,
    'items=s'            => \$items_file,
    'serials=s'          => \$serials_file,
    'serialsbranch|sb=s' => \$serials_branch,
    'transcriptor=s'     => \$transcriptor,
    'plugin=s'           => \$plugin_file,
    'output=s'           => \$output_file,
    'man'                => \$man,
    'help'               => \$help
);

pod2usage( -exitstatus => 0, -verbose => 2 ) if $man;
pod2usage(1) if $help;

die "You need to supply a biblio file (--biblios)\n" if !$biblios_file;
die "You need to supply an output file (--output)\n" if !$output_file;

my %plugins;
if ($plugin_file) {
    open my $pl_fh, '<', $plugin_file;
    my $code = read_file($pl_fh);
    %plugins = eval($code);
    die "Error reading plugin: $@" if $@;
    close $pl_fh;
}

# If an item file has been provided, preload it.
my %all_items;
if ($items_file) {
    say "Loading items...";
    %all_items = load_items($items_file);
}
my %all_serials;
if ($serials_file) {
    say "Loading serials...";
    %all_serials = load_serials( $serials_branch, $serials_file );
}

open my $out_fh, '>', $output_file;

# Now load the MARC records, and process them one-by-one.
my $marc_batch = MARC::Batch->new( 'USMARC', $biblios_file );
my $count = 0;
my $bibtype_map_plugin = $plugins{bibtype};
while ( my $marc_record = $marc_batch->next ) {
    $count++;
    print '.' if $count % 5 == 0;
    say '' if $count % 400 == 0;

    # Get the special things out of the records
    my $id         = $marc_record->subfield( '998', 'a' );
    my $recordtype = $marc_record->subfield( '998', 'b' );
    my $callnum = $marc_record->subfield('984', 'c');
    my $bibtype = $marc_record->subfield('998', 'b');

    # These records are turned into items and come from the serial data
    next if ( $recordtype eq 'SIC' );

    # Clean the record up a bit
    my @private_fields = ( $marc_record->field('9..') );
    $marc_record->delete_fields(@private_fields);
    $marc_record->insert_fields_ordered(
        MARC::Field->new( '014', ' ', ' ', 'a' => $id ) );
    $marc_record->insert_fields_ordered(
        MARC::Field->new( '040', ' ', ' ', 'c' => $transcriptor ) )
      if $transcriptor;

    # work around the fact that M::F->new requires a subfield initially.
    my $base_item = MARC::Field->new('952', ' ', ' ', '_' => 'DUMMY');
    $base_item->delete_subfield(code => '_');
    $base_item->update( 'o' => $callnum ) if ($callnum);
    # We use a plugin to map this. If there's no plugin, we skip it.
    if ($bibtype_map_plugin) {
        my $itemtype = $bibtype_map_plugin->($bibtype);
        $base_item->update('y' => $itemtype);
        $marc_record->insert_fields_ordered(
            MARC::Field->new( '942', ' ', ' ', 'c' => $itemtype ) )
    }

    # Attach items
    if ( $all_items{$id} ) {
        my @items      = @{ $all_items{$id} };
        my @items_marc = make_items_from_elm($base_item, @items);
        $marc_record->append_fields(@items_marc);
    }
    if ( $all_serials{$id} ) {
        my @items_marc = make_items_from_serials($base_item, @{ $all_serials{$id} } );
        $marc_record->append_fields(@items_marc);
    }
    fix_record($marc_record);
    # fix_isbd also tells us if there's a title field present.
    if (!fix_isbd($marc_record)) {
        warn "Skipping record for missing title\n";
        next;
    };
    clean_marc_record($marc_record);
    $plugins{marc}->($marc_record) if $plugins{marc};
    $out_fh->print( $marc_record->as_usmarc() );
}
$out_fh->close();
say '';

sub load_items {
    my ($file) = @_;
    my $csv = Text::CSV_XS->new( { auto_diag => 2, sep_char => "\t" } );
    open my $csvfile, '<', $file;
    my %h2c = make_header_index( $csv, $csvfile );

    my ( %res, $row );

    # Helper function to access fields easily.
    my $field = sub {
        return $row->[ $h2c{ shift() } ];
    };
    while ( $row = $csv->getline($csvfile) ) {
        my $bib_id = $field->('biblio unique id');
        my $item   = {
            barcode         => $field->('100_1_a_1'),
            lost            => $field->('300_1_j_1') eq 'true',
            itype           => $field->('500_1_a_1'),
            holdingbranch   => $field->('500_1_c_1'),
            homebranch      => $field->('500_1_e_1'),
            dateaccessioned => $field->('500_1_d_1'),
            itemnotes       => $field->('500_1_l_1'),
            enumchron       => $field->('500_1_n_1'),
            itemcallnumber  => $field->('500_1_o_1'),
        };
        $item = $plugins{item}->($item) if $plugins{item};
        push @{ $res{$bib_id} }, $item;
    }
    return %res;
}

sub load_serials {
    my ( $branch, $file ) = @_;
    my $csv = Text::CSV_XS->new( { auto_diag => 2, sep_char => "\t" } );
    open my $csvfile, '<', $file;
    my %h2c = make_header_index( $csv, $csvfile );
    my ( %res, $row );
    my $field = sub {
        return $row->[ $h2c{ shift() } ];
    };
    my @branch_fields;
    if ($branch) {
        push @branch_fields,
          (
            holdingbranch => $branch,
            homebranch    => $branch,
          );
    }
    while ( $row = $csv->getline($csvfile) ) {
        my $bib_id     = $field->('biblio unique id');
        my $num_copies = $field->('copies received');
        for ( 1 .. $num_copies ) {
            my $item =
              {
                enumchron => $field->('issue description'),
                @branch_fields,
              };
            $item = $plugins{serial}->($item) if $plugins{serial};
            push @{ $res{$bib_id} }, $item;
        }
    }
    return %res;
}

sub make_header_index {
    my ( $csv, $fh ) = @_;
    my $header = $csv->getline($fh);

    # Neat trick to make a map of column name indicies
    my %h2c;
    @h2c{ map lc, @$header } = 0 .. @$header;
    return %h2c;
}

sub make_items_from_elm {
    my ($base_item, @items) = @_;

    my %item_fields_map = (
        itemnumber           => '9',
        barcode              => 'p',
        dateaccessioned      => 'd',
        booksellerid         => 'e',
        homebranch           => 'a',
        price                => 'g',
        replacementprice     => 'v',
        replacementpricedate => 'w',
        datelastborrowed     => 's',
        datelastseen         => 'r',
        stack                => 'j',
        notforloan           => '7',
        damaged              => '4',
        itemlost             => '1',
        wthdrawn             => '0',
        itemcallnumber       => 'o',
        issues               => 'l',
        renewals             => 'm',
        reserves             => 'n',
        restricted           => '5',
        itemnotes            => 'z',
        holdingbranch        => 'b',
        location             => 'c',
        onloan               => 'q',
        cn_source            => '2',
        cn_sort              => '6',
        ccode                => '8',
        materials            => '3',
        uri                  => 'u',
        itype                => 'y',
        enumchron            => 'h',
        copynumber           => 't',
        stocknumber          => 'i',
    );
    my @res;
    foreach my $item (@items) {
        my $field = $base_item->clone();
        my $add_field = func( $name, $subfield ) {
            my $val = $item->{$name};
            $field->update($subfield => $val) if $val;
        };
        foreach my $col (keys %item_fields_map) {
            $add_field->($col, $item_fields_map{$col});
        }
        push @res, $field;
    }
    return @res;
}

sub make_items_from_serials {
    my ($base_item, @items) = @_;

    my @res;
    foreach my $item (@items) {
        my $field = $base_item->clone();
        my $add_field = func( $name, $subfield ) {
            my $val = $item->{$name};
            $field->update($subfield => $val) if $val;
        };
        $add_field->( 'holdingbranch', 'b' );
        $add_field->( 'homebranch',    'a' );
        $add_field->( 'enumchron',     'h' );
        push @res, $field;
    }
    return @res;
}

# This takes a record and fixes up any ISBD notation required.
# If false is returned, the record should be skipped.
sub fix_isbd {
    my ($rec) = @_;

    # For now we'll just do 245$a/b/c
    return fix_isbd_title($rec);
}

sub fix_isbd_title {
    my ($rec) = @_;

    my $field = $rec->field('245');
    return if !$field;
    my $a = $field->subfield('a');
    my $b = $field->subfield('b');
    my $c = $field->subfield('c');

    if (!$a) {
        return 0;
    }
    # We'll handle the three common cases of either of b and c, or both of
    # them.
    if ($b) {
        $a =~ s/ ?:? ?$//;
        $a .= ' : ';
    }
    elsif ($c) {
        $a =~ s| ?/? ?$||;
        $a .= ' / ';
    }
    if ( $b && $c ) {
        $b =~ s| ?/? ?$||;
        $b .= ' / ';
    }
    $b =~ s|^ ?:?/? || if $b;
    $c =~ s|^ ?:?/? || if $c;
    $field->update( 'a' => $a ) if $a;
    $field->update( 'b' => $b ) if $b;
    $field->update( 'c' => $c ) if $c;
    return 1;
}

# Misc fixes that are necessary
sub fix_record {
    my ($rec) = @_;

    # Because .eLM sometimes has the title split across multiple fields
    merge_fields('245', $rec);
    # Because serials are catalogued sometimes with the title in 222$a,
    # but we need it in 245$a
    copy_merge($rec, '222', '245', 'a');
    # Sometimes 245 is repeated for varying titles
    move_if_multiple($rec, '245', '246');
    # Koha doesn't seem to like the way some data is catalogued
    change_all_subfields($rec, '505', 'a');
}

sub merge_fields {
    my ($tag, $rec) = @_;

    my @fields = $rec->field($tag);
    return if @fields < 2;

    # We need to be careful: we should only merge them to provide a full set
    # of values. If a field has a subfield we have already seen, we do not
    # merge it or delete it. It'll be fixed up by another function.
    my @subfields = qw(a b c d e f g h i j k l m n o p q r s t u v w x y z);
    my (%vals, @fields_delete);
    FIELD: foreach my $f (@fields) {
        foreach my $subf_code (@subfields) {
            if ($vals{$subf_code} && $f->subfield($subf_code)) {
                next FIELD;
            }
        }
        foreach my $subf_code (@subfields) {
            $vals{$subf_code} = $f->subfield($subf_code) if $f->subfield($subf_code);
        }
        push @fields_delete, $f;
    }
    return if !%vals;

    my $new_field = MARC::Field->new($tag, ' ', ' ', map { $_ => $vals{$_} } sort keys %vals);
    $rec->delete_fields(@fields_delete);
    $rec->insert_fields_ordered($new_field);
}

# Copies the subfield from the first field to the second one, if the 
# second one doesn't already exist.
sub copy_merge {
    my ($rec,$from, $to, $subfield) = @_;

    my $from_field = $rec->field($from);
    return if !$from_field;
    my $value = $from_field->subfield($subfield);
    return if !$value;

    my $to_field = $rec->field($to);
    if (!$to_field) {
        $to_field = MARC::Field->new($to, ' ', ' ', $subfield => $value);
        $rec->insert_fields_ordered($to_field);
    } else {
        return if defined $to_field->subfield($subfield);
        $to_field->update($subfield => $value);
    }
}

# This converts the tags in fields from '$from' to '$to', excluding the first
# one. So if there are 3 245 fields, you can convert the second two to 246.
sub move_if_multiple {
    my ($rec, $from, $to) = @_;

    my @fields = $rec->field($from);
    return if !@fields;
    shift @fields;
    foreach (@fields) {
        $_->{_tag} = $to;
    }
}

# This takes all the subfields in the specified tag and converts them to
# the value in $to_subf, keeping existing order.
sub change_all_subfields {
    my ($rec, $tag, $to_subf) = @_;

    my @fields = $rec->field($tag);
    foreach my $f (@fields) {
        my @newsub;
        while (@{ $f->{'_subfields'} }) {
            shift @{ $f->{'_subfields'} };
            push @newsub, $to_subf;
            push @newsub, shift @{ $f->{'_subfields'} };
        }
        $f->{'_subfields'} = \@newsub;
    }
}

sub clean_marc_record {
    my ($rec) = @_;

    # We need to fix some broken characters, in particular smartquotes.
    # This means we need to dig into the guts of MARC::Field
    my @fields = $rec->fields();
    foreach my $f (@fields) {
        next if $f->is_control_field;
        die "This has no subfields!\n".Dumper($f) if !$f->{_subfields};
        my @subf = @{ $f->{_subfields} };
        my @fixed;
        while ( defined( my $code = shift @subf ) ) {
            my $s = shift @subf;
            die Dumper($rec, $f)."This has an undefined field." if !defined($s);
            push @fixed, ( $code, clean_string($s) );
        }
        $f->{_subfields} = \@fixed;
    }
    $rec->encoding('UTF-8');
}

# This does its darndest to convert the string to UTF-8.
sub clean_string {
    my ($str) = @_;
    $str =~ s/\x91/'/g;
    $str =~ s/\x92/'/g;
    $str =~ s/\x93/"/g;
    $str =~ s/\x94/"/g;
    $str =~ s/\x96/-/g;

    if ( $str =~ /\x1B|\xCC/ ) {

        # MARC-8.
        eval {
            $str = marc8_to_utf8($str);
        };
        if ($@) {
            die "Failed to convert marc8 to utf8: $@\n".Dumper($str);
        }
    }

    # Help clean up systems that don't do macrons properly
    $str =~ s/([Mm])aa?ori/$1\x{0101}ori/g;
    $str =~ s/([Ww])haa?nau/$1h\x{0101}nau/g;

    #$str = Encode::encode( 'UTF-8', $str );
    $str = fix_latin( $str );
    return $str;
}
