#!/usr/bin/perl 

# Copyright 2014 Catalyst IT Ltd.
#
# This file is not part of Koha, but it does work with it.
#
# Koha is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
#
# Koha is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Koha; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# Written by Robin Sheat <robin@catalyst.net.nz>

=head1 NAME

reviews.pl - migration of DBTextWorks reviews and rating information to Koha

=cut

# Note: this is a copy a prune of the circ migration code, so if you see
# things referring to that, that's why.

=head1 DESCRIPTION

This takes reviews and rating information from a DBTextWorks catalogue
CSV file and converts them to Koha. Every record has a key value, and it's
expected that that value is in the MARC somewhere to match it up.

This expects a single row in the catalogue to be a single item, because
that's how the data that I'm looking at works. Deduplication should happen
afterwards.

=head1 OPTIONS

=over

=item B<--dryrun>

Pretend to do things, but don't commit at the tend.

=item B<--infile>=I<filename>

The input CSV file.

=item B<--idcolumn>=I<column name>

The name of the column that contains the ID field.

=item B<--idmarc>=I<marc subfield>

The MARC field and subfield containing the ID value to match up, of the form
123_a.

=item B<--ratingcolumn>=I<column name>

The name of the column that contains the rating information. Default is
ResourceRatingBorrow.

=item B<--feedbackcolumn>=I<column name>

The name of the column that contains the user feedback. Default is
ResourceFeedback.

=cut

use autodie;
use C4::Context;
use csvutils;
use Getopt::Long;
use List::Util qw/ sum /;
use List::MoreUtils qw/ firstidx /;
use MARC::File::XML;
use Modern::Perl;
use Pod::Usage;
use Text::CSV_XS;
use Time::Piece;

my ( $infile, $idcolumn, $idmarc, $dryrun, $branchcode );
my ( $ratingcolumn, $feedbackcolumn ) = qw( ResourceRatingBorrow ResourceFeedback );
my ($help);

GetOptions(
    'dryrun'          => \$dryrun,
    'infile=s'        => \$infile,
    'idcolumn=s'      => \$idcolumn,
    'idmarc=s'        => \$idmarc,
    'ratingcolumn=s'    => \$ratingcolumn,
    'feedbackcolumn=s' => \$feedbackcolumn,
    'help'            => \$help,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage("Missing a required argument.\n")
  unless ( $infile && $idcolumn && $idmarc);
my $dbh = C4::Context->dbh;
$dbh->{AutoCommit} = 0;
$dbh->{RaiseError} = 0;

my $csv = Text::CSV_XS->new(
    {
        binary             => 1,
        eol                => $/,
        allow_loose_quotes => 0,
        auto_diag          => 2
    }
);
open my $csvfile, '<', $infile;
my $header = $csv->getline($csvfile);
my $id_idx = firstidx { $_ eq $idcolumn } @$header;
my $rating_idx = firstidx { $_ eq $ratingcolumn} @$header;
my $feedback_idx = firstidx { $_ eq $feedbackcolumn} @$header;

my $count=0;
while (my $row = $csv->getline($csvfile)) {
    if ($count % 70 == 0) {
        print "\n$count\t";
    }

    $count++;

    my $item;
    my @rating = split('\|', $row->[$rating_idx]);
    my @feedback = split('\|', $row->[$feedback_idx]);
    if (!@feedback && !@rating) {
        print '.';
        next;
    }
    print ',';
    my $id = $row->[$id_idx];
    my $biblionumber = get_biblio($id);
    if (@rating) {
        add_rating($biblionumber, @rating);
    }
    if (@feedback) {
        add_feedback($biblionumber, @feedback);
    }
}

print "\n\n";
if (!$dryrun) {
    $dbh->commit;
} else {
    $dbh->rollback;
    print "Changes not committed\n";
}

my %biblio_index;

sub get_biblio {
    my ($id) = @_;

    build_biblio_index() unless %biblio_index;
    my $bibliono = $biblio_index{$id};
    die "Unable to find a biblio for $id\n" unless $bibliono;
    return $bibliono;
}

sub build_biblio_index {
    my $dbh = C4::Context->dbh;
    my $sth = $dbh->prepare('SELECT biblionumber,marc FROM biblioitems');
    $sth->execute();
    my ($tag, $subfield) = $idmarc =~ /(\d\d\d)_(.)/;
    while (my $r = $sth->fetchrow_arrayref) {
        my $bibnum = $r->[0];
        my $marc = $r->[1];
        my $rec = eval {
            MARC::Record->new_from_usmarc($marc);
        };
        if ($@) {
            warn "Skipping the record with biblionumber $bibnum: $@\n";
            next;
        }
        my $val = $rec->subfield($tag, $subfield);
        die "No ID value found in marc record.\n".$rec->as_formatted."\n" unless $val;
        $biblio_index{$val} = $bibnum;
    }
}

sub add_rating {
    my ($bib, @ratings) = @_;

    my $mean = int(sum(@ratings) / scalar(@ratings) + 0.5);
    my $sth = $dbh->prepare('INSERT INTO ratings (borrowernumber, biblionumber, rating_value) VALUES (1, ?, ?)');
    $sth->execute($bib, $mean);
}

sub add_feedback {
    my ($bib, @feedback) = @_;

    my $sth = $dbh->prepare('INSERT INTO reviews (biblionumber, review, approved) VALUES (?,?,1)');
    foreach my $r (@feedback) {
        $sth->execute($bib, $r);
    }
}
