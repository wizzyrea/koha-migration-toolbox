#!/usr/bin/perl 
#
# Copyright 2014 Catalyst IT Ltd.
#
# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
#
# Koha is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Koha; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# Written by Robin Sheat <robin@catalyst.net.nz>

# This takes a MARC file in, creates a fake 952 record on each entry with
# options either taken literally from the command line, or from the biblio
# record.

# An example command is:
# generate_items.pl -i export_12312_UTF.mrc -o export_12312_UTF_items.mrc -m EBOOK=y -m EBL=8 -m 3=7 -m 'EBL e-Book=o' -m 'marc:856u=u'
# The left is the value, the right is the 952 subfield the value goes into. 
# If the value starts with marc: it's instead taken from the record that
# it's going to be attached to.

use autodie;

use Getopt::Long;
use IO::File;
use MARC::Batch;
use MARC::Field;
use Modern::Perl;

my (@mappings, $infile, $outfile);

GetOptions('mapping|m=s' => \@mappings, 'infile|i=s' => \$infile, 'outfile|o=s' => \$outfile);

die "Both an input and output file need to be given\n" unless $infile && $outfile;
die "At least one mapping must be provided.\n" unless @mappings;

# Generate an array of transformations that we'll apply
my @functs;
foreach my $m (@mappings) {
    push @functs, generate_transformation($m);
}

my $infh = IO::File->new($infile);
my $batch = MARC::Batch->new('USMARC', $infh);
open my $outfh, '>:utf8', $outfile;

while (my $marc = $batch->next) {
    my @item_fields;
    foreach my $f (@functs) {
        push @item_fields, $f->($marc);
    }
    my $field = MARC::Field->new('952', ' ', ' ', @item_fields);
    $marc->append_fields($field);
    print $outfh $marc->as_usmarc();
}
close $outfh;
close $infh;

# This returns a function based on the command line value. This function takes
# a record and returns a pair containing the subfield and the value to be
# part of the item.
sub generate_transformation {
    my ($defn) = @_;

    my ($val, $dest) = split /=/, $defn;
    die "Invalid field definition: $defn\n" if (!defined $val || !defined $dest);

    # if val is a literal value, that's easy. If it takes from an existing
    # MARC value, that's a little more hard, but not much.
    if ($val =~ /^marc:/) {
        return sub {
            my ($rec) = @_;
            my ($field, $subf) = $val =~ /marc:(\d\d\d)(.)/;
            my $v = $rec->subfield($field, $subf);
            return () if !$v;
            return ($dest, $v);
        };
    } else {
        return sub {
            return ($dest, $val);
        };
    }
}
