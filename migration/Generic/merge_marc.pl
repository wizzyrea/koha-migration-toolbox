#!/usr/bin/perl 
#
# This merges two MARC files together to produce one new file.
#
# Copyright 2013 Catalyst IT Ltd.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Robin Sheat <robin@catalyst.net.nz>

=head1 NAME

merge_marc.pl - takes two MARC files and merges the entries in them together

=head1 SYNOPSIS

B<merge_marc.pl>
B<--first>=I<file>
B<--second>=I<file>
B<--matchfirst|--mf>=I<marc field>
B<--matchsecond|--ms>=I<marc field>
[B<--mergeonly>=I<marc field>]
[B<--keepnomatch>]
B<--output>=I<file>
B<[--man]>
B<[--help]>

=head1 DESCRIPTION

B<merge_marc.pl> matches two MARC files by a common field, and inserts the
relevant fields from the second file into the first one. The records are
matched by looking at the B<--matchfirst> field in the first record and the
B<--matchsecond> field in the second.

=head1 OPTIONS

=over

=item B<--first>=I<file>

Specifies the first MARC file to load.

=item B<--second>=I<file>

Specifies the second MARC file to load.

=item B<--matchfirst|--mf>=T<marc field>

The MARC field to match the first file on. For example, 004 or 020a.

=item B<--matchsecond|--ms>=T<marc field>

The MARC field to match the second file on. This must be a control field.
For example, 001. This will be removed from the record before it's merged
in to the first one.

=item B<--removeitems>

Strips any existing items (952 fields) off the first file before it merges the
second file (which may or may not contain items.)

=item B<--mergeonly>

Only the specified field will be merged over, e.g. 866.

=item B<--filterfirst>

The name of a filter to apply to the first file data before attempting to
match it. Available filters are:

=over 4

=item isbn

Clean up the data to make an ISBN match more likely.

=back

=item B<--keepnomatch>

By default, any item in the first file that doesn't have a match in the
second file is discarded. This prevents that from happening.

=item B<--output>=I<file>

The file to write to.

=item B<--man>

This documentation.

=item B<--help>

A quick refresher on the options.

=back

=head1 AUTHOR

Robin Sheat <robin@catalyst.net.nz>

=cut

use autodie;
use Getopt::Long;
use MARC::File::USMARC;
use Modern::Perl;
use Pod::Usage;

use Data::Dumper;

use utf8;

my ( $file_first, $file_second, $file_out );
my ( $match_first, $match_second, $remove_items, $merge_only, $keep_no_match );
my ( $filter_first );
my ( $help, $man );

GetOptions(
    'first=s'          => \$file_first,
    'second=s'         => \$file_second,
    'output=s'         => \$file_out,
    'matchfirst|mf=s'  => \$match_first,
    'matchsecond|ms=s' => \$match_second,
    'mergeonly=s'      => \$merge_only,
    'filterfirst=s'    => \$filter_first,
    'removeitems'      => \$remove_items,
    'keepnomatch'      => \$keep_no_match,
    'man'              => \$man,
    'help'             => \$help,
);

pod2usage( -exitstatus => 0, -verbose => 2 ) if $man;
pod2usage(1) if $help;

die "Missing some required option\n"
  if !($file_first
    && $file_second
    && $file_out
    && $match_first
    && $match_second );
say "Loading second file first...";
my %index = load_index_records( $file_second, $match_second );
say "\n" . scalar( keys(%index) ) . " records loaded.\n";
say "\n\nProcessing first file...";

my $in_file = MARC::File::USMARC->in($file_first)
  || die "Unable to open file $file_first: $!\n";
open my $out_file, '>:utf8', $file_out;
my $count = 0;
while ( my $marc = eval { $in_file->next() } // $@ ) {
    if ($@) {
        warn "Failed to parse record: $@\n";
        $@ = undef;
        next;
    }
    print "\n$count " if ( $count++ % 70 == 0 );
    print '.';
    if ($remove_items) {
        my @fields = $marc->field('952');
        $marc->delete_fields(@fields);
    }
    my @match_val = filter($filter_first, get_match_data($marc, $match_first));
    my @recs_second = flatten(@index{@match_val});
    delete @index{@match_val};
    if ( !@recs_second ) {
        warn "No matches for [ ".join(', ',@match_val) . " ]\n";
        next unless $keep_no_match;
    }
    else {
        @recs_second = @recs_second;
        foreach my $sec (@recs_second) {
            $marc->append_fields( $sec->fields() );
        }
    }
    eval {
        save_marc( $out_file, $marc );
    };
    if ($@) {
        warn "Something went wrong: $@\n" . Dumper($marc);
    }
}
$in_file->close();
say "\n\n";
if (%index) {
    say scalar(keys %index) . " entries from the second file found no home:\n" . join("\n", keys %index);
} else {
    say "Done.";
}

sub load_index_records {
    my ( $file, $match ) = @_;

    my %index;
    my $in_file = MARC::File::USMARC->in($file)
      || die "Unable to open file $file: $!\n";
    my $count = 0;
    while ( my $marc = $in_file->next() ) {
        print "\n$count " if ( $count++ % 70 == 0 );
        print '.';
        my $match_field = $marc->field($match);
        die "No such field: $match\n" if !$match_field;
        my $match_val = $match_field->data();

        if ($merge_only) {

            # Delete everything except the field we keep
            my $everything_gone = 1;
            foreach my $f ( $marc->fields() ) {
                if ( $f->tag() ne $merge_only ) {
                    $marc->delete_fields($f);
                }
                else {
                    $everything_gone = 0;
                }
            }
            push @{ $index{$match_val} }, $marc unless $everything_gone;
        }
        else {
            # Delete the field we matched on
            $marc->delete_fields($match_field);
            push @{ $index{$match_val} }, $marc;
        }
    }
    return %index;
}

sub save_marc {
    my ( $fh, $marc ) = @_;

    print $fh $marc->as_usmarc();
}

sub get_match_data {
    my ($marc, $field) = @_;

    # There are two forms of field:
    # 001 or 020a (i.e. control or subfield)
    if ($field =~ /^\d\d\d$/) {
        my $f = $marc->field($field);
        die "No such field: $field\n" unless $f;
        return ($f->data());
    } elsif ($field =~ /^(\d\d\d)(.)$/) {
        my ($f, $sf) = ($1, $2);
        my @mfs= $marc->field($f);
        die "No such field: $f\n" unless @mfs;
        my @res;
        foreach my $mf (@mfs) {
            my @vals = $mf->subfield($sf);
            push @res, @vals;
        }
        return @res;
    }
}

sub flatten {
    my (@arrays) = @_;

    my @res;
    foreach my $a (@arrays) {
        next unless $a;
        push @res, @{ $a };
    }
    return @res;
}

sub filter {
    my ($filter, @data) = @_;

    my %dispatch = (
        isbn => \&filter_isbn,
    );
    map { $dispatch{$filter}->($_) } @data;
}

sub filter_isbn {
    my ($val) = @_;

    $val =~ s/^.*?([\dX]+).*$/$1/;
    return $val;
}
