#!/usr/bin/perl 
#
# Copyright 2010-2013 Catalyst IT Ltd.
#
# This file is useful in conjunction with Koha
#
# Koha is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
#
# Koha is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Koha; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# Written by Robin Sheat <robin@catalyst.net.nz>

=head1 NAME

remove_duplicate_biblios.pl - this checks the ISBN, 245$a and $b for every
record and if there are any duplicates, it deletes the second one, attaching
any items to the first.

=head1 SYNOPSIS

B<remove_duplicate_biblios.pl>
[B<--dry-run>]
[B<--man>]
[B<--help>]

=head1 OPTIONS

=over

=item B<--minlimit>

No biblios with a biblionumber less than or equal to this will be merged or
deleted. Ones above this will be merged into ones below this, but nothing under
or the same as this will be changed.

=item B<--dry-run>

Does everything except for make actual changes.

=item B<--help>

Tells you all about the options.

=item B<--man>

Tells you everything about this.

=back

=head1 DESCRIPTION

This runs against the Koha database and removes the second (and beyond)
instance of every biblio, based on a cleaned up ISBN, title and subtitle. It
will attach the items from the records being deleted to the one that's staying
behind.

It doesn't merge any parts of the record. It does
output a lot of information that can be used to review what's happening,
especially in conjunction with B<--dry-run>.

=head1 AUTHOR

Robin Sheat <robin@catalyst.net.nz>

=cut

use strict;
use warnings;

use C4::Context;
use Getopt::Long;
use MARC::Record;

use Data::Dumper;

my ( $man, $help, $dryrun, $min_limit );
my $key_field    = 'isbn';
my $key_func;
my @check_fields;

my $result = GetOptions(
    'minlimit=s'     => \$min_limit,
    'dry-run|dryrun' => \$dryrun,
    'keyfield|k=s'   => \$key_field,
    'keyfunc|kf=s'   => \$key_func,
    'checkfield|c=s' => \@check_fields,
    'help'           => \$help,
    'man'            => \$man,
);

pod2usage( -exitstatus => 0, -verbose => 2 ) if $man;
pod2usage(1) if $help;

# First build a list of all the ISBNs
my %key_to_biblio;
my %biblio_to_check;
my $count;

my $dbh = C4::Context->dbh;
$dbh->{AutoCommit}=0;
$dbh->{RaiseError}=1;
my $get_all = $dbh->prepare('SELECT biblionumber,isbn,marc FROM biblioitems');
$get_all->execute();
print "Reading all biblios\n";
while (my $record = $get_all->fetchrow_hashref) {
    print "\n$count " if $count++ % 70 == 0;
    print ".";

    $record->{marcrec} = MARC::Record->new_from_usmarc($record->{marc});
    my $key = get_key($record);
    next if !$key;
    push @{ $key_to_biblio{$key} }, $record->{biblionumber};
    $biblio_to_check{$record->{biblionumber}} = get_check($record);
}

print "\nDeleting duplicates\n";

# Start by building a list of things to update
my @tables_to_update = (
    [qw( deleteditems biblionumber )],
    [qw( deleteditems biblioitemnumber )],
    [qw( hold_fill_targets biblionumber )],
    [qw( import_biblios matched_biblionumber )],
    [qw( items biblionumber )],
    [qw( items biblioitemnumber )],
    [qw( oai_sets_biblios biblionumber )],
    [qw( old_reserves biblionumber )],
    [qw( reserveconstraints biblionumber )],
    [qw( reserveconstraints biblioitemnumber )],
    [qw( reserves biblionumber )],
    [qw( reviews biblionumber )],
    [qw( serial biblionumber )],
    [qw( subscription biblionumber )],
    [qw( subscriptionhistory biblionumber )],
    [qw( suggestions biblionumber )],
    [qw( tags_all biblionumber )],
    [qw( tags_index biblionumber )],
    [qw( virtualshelfcontents biblionumber )],
    [qw( aqorders biblionumber biblioitemnumber )],
    [qw( biblioimages biblionumber )],
);
# Some things are actually pretty complicated to fix, like ratings.
my @special_cases = (
    \&fix_ratings,
);

my @updates;
foreach my $table_row (@tables_to_update) {
    push @updates, { sth => $dbh->prepare("UPDATE $table_row->[0] SET $table_row->[1]=? WHERE $table_row->[1]=?"), fields => $table_row->[0] . ' ' . $table_row->[1] };
}

my $delete_biblio = $dbh->prepare('DELETE FROM biblio WHERE biblionumber=?');

$count=0;
my $num_deleted=0;
my $log_deleted='';
my $log_notdeleted='';
foreach my $key (sort keys %key_to_biblio) {
    print "\n$count " if $count++ % 70 == 0;
    print ".";

    my @biblionumbers = @{ $key_to_biblio{$key} };
    next if @biblionumbers < 2;

    my (@not_deleted, @deleted);
    my $primary = shift @biblionumbers;
    # Sort any special cases first
    foreach my $s (@special_cases) {
        $s->($primary, @biblionumbers);
    }
    foreach my $to_del (@biblionumbers) {
        next if defined($min_limit) && ($to_del <= $min_limit);
        # See if the check value doesn't match
        if ($biblio_to_check{$primary} ne $biblio_to_check{$to_del}) {
            push @not_deleted, $to_del;
            next;
        }
        # Run the updates
        foreach my $upd (@updates) {
            eval {
                $upd->{sth}->execute($primary, $to_del);
            };
            if ($@) {
                warn "Failed to update:\n$@\n".Dumper($upd, $primary, $to_del);
            }
        }
        $delete_biblio->execute($to_del);
        $num_deleted++;
        push @deleted, $to_del;
    }
    $log_notdeleted .= "$primary [$key]\t" . join(', ', @not_deleted). "\n" if @not_deleted;
    $log_deleted .= "$primary [$key]\t" . join(', ', @deleted). "\n" if @deleted;
}
print "\n$num_deleted biblios merged.\n";
if ($dryrun) {
    print "No changes committed.\n";
    $dbh->rollback;
} else {
    $dbh->commit;
}

print "Records merged together:\n$log_deleted\n";

print "\nRecords with a primary match, but not a check match:\n$log_notdeleted\n";

sub clean_isbn {
    my ($dirty) = @_;

    $dirty =~ /(\d{9,13})/;
    return $1;
}

sub get_title {
    my ($rec) = @_;

    my $a = $rec->subfield('245', 'a') // '';
    my $b = $rec->subfield('245', 'b') // '';

    $a =~ s/\s?[:\/]?\s*$//;
    $b =~ s/\s?[:\/]?\s*$//;
    return "$a $b";
}

sub get_key {
    my ($rec) = @_;

    # Special case for isbn
    if ($key_field eq 'isbn') {
        return clean_isbn($rec->{isbn});
    }

    my $marc = $rec->{marcrec};
    my ($f, $sf) = $key_field =~ /^(\d\d\d)(.)$/;
    $_ = $marc->subfield($f, $sf);
    eval $key_func if $key_func && defined $_;
    return $_;
}

sub get_check {
    my ($rec) = @_;

    # Create our check value by glomming each field together
    my @vals;
    foreach my $cf (@check_fields) {
        my $val;
        if ( $cf eq 'title' ) {
            $val = get_title( $rec->{marcrec} );
        }
        else {
            my ( $f, $sf ) = $cf =~ /^(\d\d\d)(.)$/;
            die "\n$cf is not a valid field\n" if !defined $f || !defined $sf;
            $val = $rec->{marcrec}->subfield( $f, $sf );
        }
        push @vals, $val if defined $val;
    }
    return join '--', @vals;

    return join '--',
      grep { defined }
      map { $rec->{marcrec}->subfield( $_->[0], $_->[1] ) }
      map { [/^(\d\d\d)(.)$/] }
      map { $_ eq 'title' ? get_title($rec->{marcrec}) : $_ } @check_fields;
}

# Because these have a joined primary key, if we deduplicate we can cause
# collisions. So we mean them together. It's not perfect, but the best we
# can do.
sub fix_ratings {
    my (@biblios) = @_;

    # Mean together the values
    my $sth = $dbh->prepare('SELECT rating_value FROM ratings WHERE biblionumber IN ('.join(',', map {'?'} @biblios).')');
    $sth->execute(@biblios);
    my ($total, $count) = (0, 0);
    while (my $r = $sth->fetchrow_arrayref) {
        $total += $r->[0];
        $count++;
    }
    return if $total == 0;
    my $mean = int($total / $count + 0.5);

    # Now delete the rating and reinsert one
    $sth = $dbh->prepare('DELETE FROM ratings WHERE biblionumber IN ('.join(',', map {'?'} @biblios).')');
    $sth->execute(@biblios);

    # And finally insert the new one with borrower 1
    $sth = $dbh->prepare('INSERT INTO ratings (borrowernumber, biblionumber, rating_value) VALUES (1, ?, ?)');
    $sth->execute(shift @biblios, $mean);
}
