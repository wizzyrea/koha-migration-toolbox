#!/usr/bin/perl 

# Converts Symphony MARC-CSV files to MARC records

# Copyright (C) 2012 Catalyst IT Ltd.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# This takes a MARC-CSV file and converts it into MARC to be
# imported as biblios or authorities

=head1 NAME

marc_from_csv.pl - Convert Symphony MARC-CSV to MARC

=head1 SYNOPSIS

B<marc_from_csv.pl>
B<--input> I<csv_file.csv>
B<--output> I<file.marc>
[B<--ignore> I<offset>]
[B<--man>]
[B<--help>]

=head1 DESCRIPTION

Takes the CSV MARC dump from Symphony and turns it into MARC records to be
imported into Koha.

=head1 OPTIONS

=over 4

=item B<--input> I<csv_file.csv>

The file to load the MARC from. Typically this comes from the AUTHORVED or
MARC0 table.

=item B<--output> I<file.marc>

The output file.

=item B<--ignore> I<offset>

Ignore any records with the supplied offset value, usually because they're
problematic. This can be repeated, or comma separated.

=item B<--man>

Full documentation.

=item B<--help>

A usage synopsis.

=back

=head1 AUTHOR

Robin Sheat <robin@catalyst.net.nz>

=cut

use Data::Dumper;
use Getopt::Long;
use MARC::Field;
use MARC::Record;
use Modern::Perl;
use Pod::Usage;
use Text::CSV_XS;

my @ignores;
my %opts = ( 'ignore' => \@ignores );

my $opt_res =
  GetOptions( \%opts, 'input|i=s', 'output|o=s', 'ignore=s', 'man', 'help|h' );

pod2usage( { -exitval => 1, -verbose => 0, -output => \*STDERR } )
  if ( !$opt_res );
pod2usage( { -exitval => 0, -verbose => 2 } ) if ( $opts{man} );
pod2usage( { -exitval => 0, -verbose => 1 } ) if ( $opts{help} );

my $err =
    !$opts{input}  ? "Input file missing (--input)"
  : !$opts{output} ? "Output MARC filename missing (--output)"
  :                  undef;

pod2usage(
    { -exitval => 1, -verbose => 0, -output => \*STDERR, -message => $err } )
  if ($err);

my %ignore = map { $_ => 1 } split( /,/, join( ',', @ignores ) )
  if @ignores;

my $csv = Text::CSV_XS->new();
open my $fh, "<:encoding(utf8)", $opts{input};
open my $out_fh, '>', $opts{output};

# Grab the header and put it into a hash so we can use field names.
my $header = $csv->getline($fh);
my $count  = 0;
my %h2c    = map { $_, $count++ } @$header;
my %stats;
my %seen;    # This lets me know if records are split up within the file
$count = 1;
my %records;    # Store them here so they can be saved afterwards

while ( my $row = $csv->getline($fh) ) {

    if ( $count % 70 == 0 ) {
        print "\r$count ";
    }
    else {
        print ".";
    }
    $count++;
    my $offset = $row->[ $h2c{OFFSET} ];
    next if $ignore{$offset};
    my $tag_number = $row->[ $h2c{TAG_NUMBER} ];
    my $ind1       = $row->[ $h2c{INDICATOR1} ];
    my $ind2       = $row->[ $h2c{INDICATOR2} ];
    my $tag        = $row->[ $h2c{TAG} ];

    $tag_number = sprintf( "%03d", $tag_number ) if length($tag_number) != 3;
    my $marc_record;
    if ( !$records{$offset} ) {
        $marc_record = $records{$offset} = MARC::Record->new();
    }
    else {
        $marc_record = $records{$offset};
    }
    if ( $tag_number eq '000' ) {

        # It's a leader
        next;

        # This code here doesn't work due to the length in the record
        # being too short, but there's a small chance that it'll have to be
        # made to work so I'm keeping it for now.
        my $l_val = decode_tag($tag)->{a};
        die "Undefined leader value, offset $offset" if !$l_val;
        $marc_record->leader($l_val);
    }
    else {
        my $field = eval {
            MARC::Field->new( $tag_number, $ind1, $ind2, decode_tag($tag) );
        };
        if ($@) {
            die "$@: " . Dumper($row);
        }
        $marc_record->append_fields($field);
    }
}
print "\n";
print "Saving records.\n";
foreach my $offset ( keys %records ) {
    save_record( $records{$offset} );
    $stats{'Records Output'}++;
}

foreach my $s ( keys %stats ) {
    print $s . "\t" . $stats{$s} . "\n";
}

close($fh);
close($out_fh);

sub save_record {
    my ($rec) = @_;

    print $out_fh $rec->as_usmarc();
}

sub decode_tag {
    my ($tag) = @_;

    # Some of the records don't have a subfield letter. They also don't
    # start with a '|', so if we see them we assume it's 'a'.
    if ( $tag !~ /^\|/ ) {
        return ( a => $tag );
    }
    my @parts = split( '\|', $tag );
    my @result;
    foreach my $p (@parts) {
        next if !$p;
        my $code = substr $p, 0, 1;
        my $value = substr $p, 1;
        push @result, $code, $value;
    }
    return @result;
}
