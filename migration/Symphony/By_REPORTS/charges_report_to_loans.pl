#!/usr/bin/perl 

# Converts Symphony "charges report" to SQL for Koha current loans.

# Copyright (C) 2012 Catalyst IT Ltd.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# For some reason, it's hard to get current loans data out of Symphony. Instead,
# you get it from a charges report which is a horrible text format. This
# processes that file (an annotated example of which is included alongside
# this script) and outputs SQL that'll add entries to the Koha issues table.

use autodie;

use Data::Dumper;
use Getopt::Long;
use Modern::Perl;

my ( $infile, $outfile, $help );

GetOptions(
    'input=s'  => \$infile,
    'output=s' => \$outfile,
    'help|h'   => \$help,
);

if ($help) {
    show_help();
    exit;
}

if ( !$infile || !$outfile ) {
    show_help("Both --input and --output must be provided.");
    exit(1);
}

open( my $infh,  '<', $infile );
open( my $outfh, '>', $outfile );

my %stats;

# This part will collect up parts seperated by blank lines and send
# them on to be processed.
my $block;
my $state = 'OUT';
while ( my $line = <$infh> ) {
    $stats{Lines}++;
    next if $line =~ /^\s*$/;
    if ( $state eq 'OUT' ) {
        if ( $line =~ /copy:/ ) {

            # Start of a block
            $stats{'Attempted Blocks'}++;
            $block = $line;
            $state = 'IN';
        }
    }
    elsif ( $state eq 'IN' ) {
        next if $line =~ /^\s+Charge List\s*$/;
        next if $line =~ /^\s{4,}Produced /;

        $block .= $line;

        # Test for last line, which is usually something like:
        #                     ON-LOAN             WELLINGTON
        if ( $line =~ /^\s+[-A-Z]+\s+[-A-Z]+\s*$/ ) {
            my $record = parse_block($block);
            $stats{'Valid Blocks'}++ if $record;
            save_record( $outfh, $record ) if $record;
            $state = 'OUT';
        }
    }
    else {
        die "Unknown state: $state";
    }
}

# This ensures that items are updated appropriately
print $outfh "UPDATE items,issues SET items.onloan=issues.issuedate WHERE issues.itemnumber=items.itemnumber;\n";
foreach my $stat ( sort keys %stats ) {
    say "$stat:\t$stats{$stat}";
}

close $infh;
close $outfh;

# This will attempt to parse a block that is hopefully a charge record
# and output a hash containing the stuff that we care about.
sub parse_block {
    my ($block) = @_;

    # First check if it's valid
    return undef if !valid_block($block);

    # If other versions of symphony output things differently, it would be
    # quite possible to make this parser smarter by joining up lines to
    # form a better record.

    # Now we get the first, and the last three rows from it.
    my ( $first, $second, $third, $fourth ) =
      $block =~ m/^(.*?)\n.*\n(.*)\n(.*)\n(.*)\n$/s;

    # If this fails, we'll need to change what we think is valid.
    die "Invalid block: $block\n" if ( !$first );

    # First row contains call number (free-form), copy number, barcode/ID
    # (which are typically the same thing)
    my ( $call, $copy, $barcode ) = $first =~ m/^(.*)  copy:([^ ]*)  (.*?)\s*$/;

    # Second row contains user, userid, date
    my ( $user, $userid, $issue_date ) =
      $second =~ m/^  (.*?)\s{2,}([A-Z0-9]+?)\s+(.*)$/;

    # Third row contains due date, renewed, renewals, overdue, recalled
    my ( $due_date, $renewed_date, $renewed, $renewals, $overdue, $recalled ) =
      $third =~ m/^([^ ]+)(?:\s+(\d+\/\d+\/\d+),\d+:\d+)?(?:\s+(\d+))(?:\s+(\d+))(?:\s+(\d+))(?:\s+(\d+))\s*$/;

    # Fourth row contains location and library
    my ( $location, $library ) = $fourth =~ m/^\s+(.*?)\s\s+(.*?)\s*$/;

    my %result = (
        call_number => $call,
        copy_number => $copy,
        barcode     => $barcode,
        user        => $user,
        userid      => $userid,
        issue_date  => fix_date($issue_date),
        due_date    => fix_date($due_date),
        renewed     => $renewed,
        renewals    => $renewals,
        overdue     => $overdue,
        recalled    => $recalled,
        location    => $location,
        library     => $library,
    );
#    die Dumper(\%result, $due_date, $renewed, $renewals, $overdue, $recalled, $third) if $result{location} eq 'ON-LOAN';
    return \%result;
}

# Runs a few quick tests to see if the provided block is a valid record
sub valid_block {
    my ($block) = @_;

    return 0 unless $block =~ /copy:/;

    return 1;
}

# Convert a date from symphony to SQL form
sub fix_date {
    my ($date) = @_;

    return undef if ( !$date );

    return '2099-01-01' if $date eq 'NEVER';

    my ( $dd, $mm, $yy ) = $date =~ m#(\d?\d)/(\d?\d)/(\d\d\d\d)#;
    if ( !defined($dd) || !defined($mm) || !defined($yy) ) {
        warn "Unparsable date: $date\n";
        return undef;
    }
    return "$yy-$mm-$dd";
}

# Output the extracted data
sub save_record {
    my ( $fh, $record ) = @_;

    if ( $record->{location} eq 'ON-LOAN' ) {
        # Sanity check
        my @undefs = grep { !defined $record->{$_} } qw/ barcode userid due_date issue_date library /;
        if (@undefs) {
            die(@undefs." fields were undef: @undefs\n" . Dumper($record));
        }

        print $fh
"INSERT INTO issues (borrowernumber, itemnumber, date_due, branchcode, issuingbranch, issuedate) VALUES (";
        print $fh "(SELECT borrowernumber FROM borrowers WHERE userid='".sql_escape($record->{userid})."'), ";
        print $fh "(SELECT itemnumber FROM items WHERE barcode='".sql_escape($record->{barcode})."'), ";
        print $fh "'" . join("','", @{$record}{'due_date', 'library', 'library', 'issue_date'}) . "');\n";
    }
}

sub sql_escape {
    my $str = shift;

    $str =~ s/'/\\'/g;
    $str =~ s/"/\\\"/g;
    return $str;
}

sub show_help {
	my $msg = shift;

	say "$msg " if $msg;
	print <<EOH;
Converts the text file report of charges produced by SirsiDynix's Symphony that
contains the current issues, and converts them into SQL that can be loaded
into Koha to populate the issues table.

Options:
    --input     the input file
    --output    the output file
    --help      this information
EOH
}
