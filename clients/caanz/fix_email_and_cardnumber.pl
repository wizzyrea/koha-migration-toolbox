#!/usr/bin/perl 
#===============================================================================
#
#         FILE: import_circ.pl
#
#        USAGE: ./import_circ.pl
#
#  DESCRIPTION: script for importing the corrections circ data
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Chris Cormack (rangi), chrisc@catalyst.net.nz
# ORGANIZATION: Koha Development Team
#      VERSION: 1.0
#      CREATED: 04/04/13 11:46:37
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use Text::CSV_XS;
use Getopt::Long;
use Date::Manip;

use C4::Context;

my $input_file;
GetOptions( 'input|i=s' => \$input_file, );

my $csv = Text::CSV_XS->new(
    {
        binary             => 1,
        eol                => $/,
        allow_loose_quotes => 1,
        escape_char        => '',
        sep_char           => ',',
        quote_char         => '"',
        auto_diag          => 2,
    }
);

my $dbh          = C4::Context->dbh();
my $borrower_sth = $dbh->prepare(
"UPDATE borrowers set email = ?, address=?, city=?, state=?, zipcode=?, country=? WHERE cardnumber = ?"
);
open my $csvfile, '<', $input_file or die "Unable to open $input_file: $!\n";

Date_Init("DateFormat=non-US");

while ( my $row = $csv->getline($csvfile) ) {
    $borrower_sth->execute( $row->[3], $row->[4], $row->[5], $row->[6],
        $row->[7], $row->[8], 'A' . $row->[0] );
    print $row->[3] . "\t" . 'A' . $row->[0] . "\n";
}
