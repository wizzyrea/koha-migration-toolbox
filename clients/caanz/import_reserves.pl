#!/usr/bin/perl 
#===============================================================================
#
#         FILE: import_reserves.pl
#
#        USAGE: ./import_reserves.pl
#
#  DESCRIPTION: script for importing the corrections circ data
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Chris Cormack (rangi), chrisc@catalyst.net.nz
# ORGANIZATION: Koha Development Team
#      VERSION: 1.0
#      CREATED: 04/04/13 11:46:37
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use Text::CSV_XS;
use Getopt::Long;
use Date::Manip;

use C4::Context;

my $input_file;
GetOptions( 'input|i=s' => \$input_file, );

my $csv = Text::CSV_XS->new(
    {
        binary             => 1,
        eol                => $/,
        allow_loose_quotes => 1,
        escape_char        => '',
        sep_char           => ',',
        quote_char         => '"',
        auto_diag          => 2,
    }
);

my $dbh          = C4::Context->dbh();
my $borrower_sth = $dbh->prepare(
"SELECT borrowernumber, firstname, surname FROM borrowers WHERE cardnumber = ?"
);
my $item_sth =
  $dbh->prepare("SELECT itemnumber,biblionumber FROM items WHERE barcode = ?");
open my $csvfile, '<', $input_file or die "Unable to open $input_file: $!\n";

Date_Init("DateFormat=non-US");

my $reserves_sth = $dbh->prepare(
"INSERT INTO reserves (borrowernumber, itemnumber, biblionumber, constrainttype, branchcode, priority) 
VALUES (?,?,?,?,?,?)"
);

while ( my $row = $csv->getline($csvfile) ) {
    if ( $row->[7] ) {
        my @reserves = split( /\|/, $row->[7] );
        my $priority = 1;
        foreach my $reserve (@reserves) {
            if ( $reserve =~ /\[(.*?)\]/ ) {
                $borrower_sth->execute( 'A' . $1 );
                my $borrower = $borrower_sth->fetchrow_hashref();
                if ($borrower) {
                    $item_sth->execute( $row->[5] );
                    my $item = $item_sth->fetchrow_hashref();
                    if ($item) {
                      $reserves_sth->execute($borrower->{borrowernumber}, $item->{itemnumber}, $item->{biblionumber},'O','AUS', $priority);
                      $priority++;
                    }
                    else {
                        print "Can't find item " . $row->[5] . "\n";
                    }
                }
                else {
                    print "Cant find borrower " . $1 . "\n";
                }
            }
        }
    }
}
