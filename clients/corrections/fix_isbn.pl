#!/usr/bin/perl 
#===============================================================================
#
#         FILE: fix_isbn.pl
#
#        USAGE: ./fix_isbn.pl
#
#  DESCRIPTION: fix for adding missing isbn
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Chris Cormack (rangi), chrisc@catalyst.net.nz
# ORGANIZATION: Koha Development Team
#      VERSION: 1.0
#      CREATED: 04/04/13 11:46:37
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use Text::CSV_XS;
use Getopt::Long;
use Date::Manip;

use C4::Context;
use C4::Biblio;

my $input_file;
GetOptions( 'input|i=s' => \$input_file, );

my $csv = Text::CSV_XS->new(
    {
        binary             => 1,
        eol                => $/,
        allow_loose_quotes => 1,
        escape_char        => '',
        sep_char           => ',',
        quote_char         => '"',
        auto_diag          => 2,
    }
);

open my $csvfile, '<', $input_file or die "Unable to open $input_file: $!\n";

my $select =
"select biblionumber,  ExtractValue(marcxml, '//datafield[\@tag=\"024\"]/subfield[\@code>=\"a\"]') AS control, ExtractValue(marcxml, '//datafield[\@tag=\"020\"]/subfield[\@code>=\"a\"]') AS isbn from biblioitems having control=?;";

my $select_sth = C4::Context->dbh->prepare($select);

while ( my $row = $csv->getline($csvfile) ) {
    $select_sth->execute( $row->[0] );
    my $data = $select_sth->fetchrow_hashref();
    if ( !$data->{isbn} ) {
        my $record = GetMarcBiblio( $data->{biblionumber} );
        if ($record) {
            print $row->[0] . "\t"
              . $data->{biblionumber} . "\t"
              . $row->[1] . "\t"
              . $data->{isbn} . "\n";
            my $f020 = MARC::Field->new( '020', '1', '0', 'a' => $row->[1] );
            $record->add_fields($f020);
            ModBiblio( $record, $data->{biblionumber} );

        }
    }
}

