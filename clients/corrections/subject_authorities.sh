#!/bin/bash

../../migration/Generic/csvtomarc.pl \
        -m 'func:literal:CORR=marc:003' \
        -m Term=marc:150_a? \
	-m 'func:split:|:RT=marc:550_a?' \
	-m 'func:split:|:UF=marc:450_a?' \
	-m 'func:split:|:BT=marc:550_a?%' \
	-m 'func:literal:g=marc:550_w' \
	-m 'func:split:|:NT=marc:550_a?%' \
	-m ScopeNote=marc:680_a? \
        --kohaconf /home/chrisc/koha-dev/etc/koha-conf.xml \
        --format usmarc     \
        -o koha_auth.marc     \
        -i data.csv     \
        -v
#   -m 'func:literal:h=marc:550_w' \

