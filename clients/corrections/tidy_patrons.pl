#!/usr/bin/perl 
#===============================================================================
#
#         FILE: tidy_patrons.pl
#
#        USAGE: ./tidy_patrons.pl
#
#  DESCRIPTION:
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Chris Cormack (rangi), chrisc@catalyst.net.nz
# ORGANIZATION: Koha Development Team
#      VERSION: 1.0
#      CREATED: 03/04/13 13:45:09
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use Text::CSV_XS;
use Getopt::Long;

my $input_file;
GetOptions( 'input|i=s' => \$input_file, );

my $csv = Text::CSV_XS->new(
    {
        binary             => 1,
        eol                => $/,
        allow_loose_quotes => 1,
        escape_char        => '',
        sep_char           => ',',
        quote_char         => '"',
        auto_diag          => 2,
    }
);

open my $csvfile, '<', $input_file or die "Unable to open $input_file: $!\n";
open my $OUTFILE, '>', 'output';

my $count = 0;
while ( my $row = $csv->getline($csvfile) ) {

    if ( $count == 0 ) {

        #first row ditch it and print our own
        print $OUTFILE
"cardnumber,surname,firstname,othernames,address,city,email,phone,phonepro,categorycode,borrowernotes,patron_attributes,contactnotes\n";
    }
    else {
        $row->[2] =~ s/\(.*\)//;
        # $row->[3] =~ s/\(.*\)//;
        $row->[4] =~ s/\|/<br \/>/g;

        #
        $row->[9] =~ s/External Library/ILL/;
        $row->[9] =~ s/Corrections Services/STAFF/;
        $row->[9] =~ s/Service Development/DEV/;
        $row->[9] =~ s/Governance & Assurance/GOV/;
        $row->[9] =~ s/Human Resources/STAFF/;
        $row->[9] =~ s/Finance, Technology & Commercial/STAFF/;
        
        $row->[11] = "BTYPE:" . $row->[11];
        my $status = $csv->print( $OUTFILE, $row );
    }
    $count++;
}
