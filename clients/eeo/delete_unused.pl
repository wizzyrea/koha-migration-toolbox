#!/usr/bin/perl 

use strict;
use warnings;

use Getopt::Long;
use Text::CSV;
my $debug   = 0;
my $doo_eet = 0;
$| = 1;

my $input_file  = "";
my $err_file    = "";
my $output_file = "";

GetOptions(
    'in=s'  => \$input_file,
    'err=s' => \$err_file,
    'debug' => \$debug,
);

if ( ( $input_file eq '' ) || ( $err_file eq '' ) ) {
    print "Something's missing.\n";
    exit;
}

my $i               = 0;
my $attempted_write = 0;
my $written         = 0;
my $other_problem   = 0;
my $broken_records  = 0;

my $csv = Text::CSV->new( { binary => 1 } );

open my $in,  "<$input_file";
open my $err, ">$err_file";

my $headerline = $csv->getline($in);

my $status;

my %barcodes;
while ( my $row = $csv->getline($in) ) {
    $barcodes{ $row->[3] } = 1;
}

use Data::Dumper;
warn Dumper \%barcodes;

use C4::Context;
my $dbh = C4::Context->dbh;

my $query = "SELECT barcode FROM items";
my $sth   = $dbh->prepare($query);
$sth->execute;
my $count = 0;
while ( my $barcode = $sth->fetchrow_hashref() ) {
    if ( exists $barcodes{ $barcode->{'barcode'} } ) {
        print "Keep this one \n";
        $count++;
    }
    else {
        print "Delete this one " . $barcode->{'barcode'} . "\n";
    }
}

print "\n\n $count items kept";
