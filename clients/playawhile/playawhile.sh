#!/bin/bash

../../migration/Generic/csvtomarc.pl \
-m 'ToyId=marc:024_a' \
-m 'Category=marc:650_a' \
-m 'Name=marc:245_a' \
-m 'Description=marc:520_a?' \
-m 'Age=marc:307_a?' \
-m 'Pieces=marc:300_a' \
-m 'Picture=marc:856_u?' \
-m 'ToyId=marc:952_p' \
-m 'func:literal:TOY=marc:952_a' \
-m 'func:literal:TOY=marc:952_b' \
-m 'func:literal:image/jpeg=marc:856_q' \
        --kohalibs=/home/chrisc/catalyst-koha \
        --kohaconf /home/chrisc/koha-dev/etc/koha-conf.xml \
        --format usmarc     \
        -o koha.marc     \
        -i toys.csv     \
        -v

        #-m year=publicationyear? \ # source has publicationdate, no year. Could strip out.
        #-m 'classification2=skipif:^INTERLOAN'  \
        #-m country=place?   \
        #-m 'func:prefix:Article 1: :article1=append:notes?'   \
        #-m 'func:prefix:Article 2: :article2=append:notes?'   \
        #-m 'func:prefix:Article 3: :article3=append:notes?'   \
        #-m 'func:prefix:Article 4: :article4=append:notes?'   \
        #-m 'func:prefix:Article 5: :article5=append:notes?'   \
        #-m 'func:prefix:Article 6: :article6=append:notes?'   \
        #-m 'func:prefix:Journal details: :jdetails=append:notes?' \
        #-m 'func:splitcount:;:accession=special:count?' \
        #-m 'func:split:|:subject=marc:650_a?'   \
        #-m 'func:split:|:keywords=marc:651_a?'  \

# -m PublicationDate 008
# split publisher
