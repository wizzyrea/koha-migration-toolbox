#!/bin/bash
perl -I ../../migration/Generic ../../migration/Generic/csvtomarc.pl \
    -i catalogue.csv \
    -o parlib.marc \
    -v  \
    -f usmarc \
    -m 'CatID=marc:998_b' \
    -m 'CatCallNumber=marc:082_a?' \
    -m 'CatTitle=special:title?' \
    -m 'CatAlternateTitle=marc:246_a?' \
    -m 'CatAuthor=marc:100_a?' \
    -m 'CatCorpAuthor=marc:110_a?' \
    -m 'CatResponsibility=marc:245_c?' \
    -m 'CatEdition=marc:250_a?' \
    -m 'func:split:::CatPublisher=multimarc:260_a:260_b:?' \
    -m 'CatDatePublished=marc:260_c?' \
    -m 'CatPhysDesc=marc:300_a?' \
    -m 'CatSeries=marc:830_a?' \
    -m 'func:split:|:CatSubjects=marc:650_a?' \
    -m 'CatAbstract=marc:520_a?' \
    -m 'CatNotes=marc:500_a?' \
    -m 'CatISBN=marc:020_a?' \
    -m 'CatISSN=marc:022_a?' \
    -m 'CatLanguage=marc:546_a?' \
    -m 'CatSerHoldings=marc:362_a?' \
    -m 'CatURL=marc:856_u?' \
    -m 'CatUrlNotes=marc:856_z?' \
    -m 'CatSeriesDesc=marc:490_a?'\
    -m 'func:split:|:CatOtherPerson=marc:700_a?' \
    -m 'func:split:|:CatOtherCorp=marc:710_a?' \
    -m 'func:split:|:CatRecordType=multimarc:942_c:952_8?' \
    -m 'func:split:|:CatLocation=marc:852_a?' \
    -m 'func:split:|:CatStatus=marc:852_z?' \
    -m 'CatSerRetention=marc:866_a?' \
    -m 'item:ItemBarCode=barcode?' \
    -m 'item:ItemCopyInfo=stocknumber?' \
    -m 'item:ItemCallNumber=itemcallnumber?' \
    -m 'item:ItemLocation=location?' \
    -m 'item:ItemSubLocation=itemnotes?' \
    -m 'item:ItemVolume=copynumber?' \
    -m 'func:literal:PS=marc:952_a' \
    -m 'func:literal:PS=marc:952_b' \
    --itemlink 'CatId=ItemCatID' \
    -t 'items.csv'

### To map
# CatDateCataloged ?
# CatDateApproved  ?
# CatDateCreated
# CatDateModified
# CatWebSiteHeading
# CatWebSiteMP
# CatWebSiteMP


### skipped as not useful
# CatSerFrequency
# CatSerIssuesPerVol
# CatAuthorRef
# CatCorpAuthorRef
# CatSeriesRef
# CatSubjectsRef
# CatOtherPersonRef
# CatOtherCorpRef
# CatFlag

### skipped as empty
# CatEditor
# CatSource
# CatPlace
# CatLCCard
# CatImage
# CatFileName
# CatFullText
# CatDocsBulletin
# CatSerSpecialIssues
# CatSerIndexedIn

# NOTES
# Cant split subjects across marc, no way to know
