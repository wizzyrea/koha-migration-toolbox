my %busunit = (
    'ARM Clients'                                     => 'ARM',
    'CE\'s Office'                                    => 'CE-OFF',
    'IABC Registered Clients'                         => 'IABC',
    'IAC Registered Clients'                          => 'IAC',
    'IKS, Archives'                                   => 'IKS-ARC',
    'IKS, CE\'s Office'                               => 'IKS-CE',
    'IKS, GIS'                                        => 'IKS-GIS',
    'IKS, National Library'                           => 'IKS-NATLIB',
    'Interloans'                                      => 'INTERLOAN',
    'MCDEM'                                           => 'MCDEM',
    'PREA, CE\'s Office'                              => 'PREA-CE',
    'PREA, Crown Entity Monitoring & Appointments'    => 'PREA-CEMA',
    'PREA, Ethnic Advisory'                           => 'PREA-EA',
    'PREA, Ethnic Policy'                             => 'PREA-EP',
    'PREA, GM Support'                                => 'PREA-GM',
    'PREA, Intercultural Advisory'                    => 'PREA-IA',
    'PREA, Language Line'                             => 'PREA-LL',
    'PREA, National Operations'                       => 'PREA-NO',
    'PREA, Office of Ethnic Affairs'                  => 'PREA-OEA',
    'PREA, Policy'                                    => 'PREA-POL',
    'PREA, PREA Branch Development & Support'         => 'PREA-BDS',
    'PREA, Regulatory Services'                       => 'PREA-RS',
    'SDO, BDM'                                        => 'SDO-BDM',
    'SDO, Business Development'                       => 'SDO-BD',
    'SDO, CE\'s Office'                               => 'SDO-CE',
    'SDO, Charities'                                  => 'SDO-CHAR',
    'SDO, Citizenship'                                => 'SDO-CITZ',
    'SDO, Community Operations'                       => 'SDO-CO',
    'SDO, Customer Service'                           => 'SDO-CS',
    'SDO, Identity and Data Services'                 => 'SDO-IDS',
    'SDO, Passports'                                  => 'SDO-PASS',
    'SDO, SDO Branch Development & Support'           => 'SDO-BDS',
    'SG, CE\'s Office'                                => 'SG-CE',
    'SG, Legal'                                       => 'SG-LEGAL',
    'SG, Research & Evaluation'                       => 'SG-RE',
    'SG, Risk Assurance & Audit'                      => 'SG-RAA',
    'SG, SG Branch Development & Support'             => 'SG-BDS',
    'SG, Strategy Planning & Performance'             => 'SG-SPP',
    'SS, Administration Services'                     => 'SS-AS',
    'SS, CE\'s Office'                                => 'SS-CE',
    'SS, Communications'                              => 'SS-COMM',
    'SS, Finance'                                     => 'SS-FIN',
    'SS, Human Resources'                             => 'SS-HR',
    'SS, Ministerial Services & Secretariat Services' => 'SS-MSSS',
    'SS, Royal Commission Inquiry - Pike River'       => 'SS-PIKE',
    'SS, Shared Services Operations'                  => 'SS-SSO',
    'SS, SS Branch Development & Support'             => 'SS-BDS',
    'SST, CE\'s Office'                               => 'SST-CE',
    'SST, Govt ICT Strategy & Planning'               => 'SST-GICTSP',
    'SST, Govt ICT Supply Management Office'          => 'SST-GICTSMO',
    'SST, GTS'                                        => 'SST-GTS',
    'SST, Service Transformation'                     => 'SST-ST',
);

sub fix_dept_code {
    my ($map, $code, $value) = @_;
    # This does custom mapping from the codes in the dia data to authorised
    # value codes.
    if (!defined($value)) {
        warn "No such department code: $value" if (!defined($value));
        return ($code, 'UNKNOWN');
    }
    my $new_val = $map->{$value};
    return ($code,$new_val);
}

( attributes =>
      { department => curry( \&fix_dept_code, \%busunit, 'BUSUNIT' ), } );
