my %br = (
    WGTN => 'ARWEL',
    AUCK => 'ARAKD',
    CHCH => 'ARCHCH',
    DUD  => 'ARDUD',
);

sub fix_branch_code {
    my ($item) = @_;

    my $b;

    $b = $br{ $item->{holdingbranch} };
    die "Unknown branch: $b" if !$b;
    $item->{holdingbranch} = $b;
    $item->{homebranch} = $b; # we make these identical deliberately

    return $item;
}

my %it = (
    ARC  => 'M',
    CD   => 'V',
    NZ   => 'M',
    RARE => 'M',
    REF  => 'M',
    V    => 'V',
);

# We convert the eLM item types to Liberty ones
sub fix_item_types {
    my ($item) = @_;

    my $b = $it{ $item->{itype} };
    die "Unknown item type: " . $item->{itype} . "\n" if !$b;

    $item->{itype} = $b;
    return $item;
}

my %cc = (
    ARC  => 'TEC',
    NZ   => 'BK',
    REF  => 'REF',
    RARE => 'RARE',
    CD   => 'MULTI',
    V    => 'BK',
);

sub set_collection_code {
    my ($item) = @_;

    my $cc = $cc{ $item->{itype} };
    die "Unknown item type for ccode: " . $item->{itype} . "\n" if !$cc;

    $item->{ccode} = $cc;
    return $item;
}

sub fix_item_things {
    set_collection_code(@_);
    fix_branch_code(@_);
    fix_item_types(@_);
}

my %bibtype = (
    '001' => 'M',
    '002' => 'S',
    '003' => 'A',
    '006' => 'V',
    '010' => 'V',
    'SIC' => '--This should never make it to an output record--',
);

sub map_bibtype {
    my ($bibtype) = @_;

    my $res = $bibtype{$bibtype};
    die "Unknown bibtype: $bibtype\n" if !defined($res);
    return $res;
}

sub fix_serials {
    my ($item) = @_;

    $item->{ccode} = 'JOUR';
    $item->{itype} = 'S';
    return $item;
}

sub set_analytics {
    my ($rec) = @_;

    my $leader = $rec->leader();
    substr($leader,19,1) = ' ';
    $rec->leader($leader);
}

sub fix_marc {
    my ($record) = @_;

    set_analytics($record);
}

(
    item    => \&fix_item_things,
    bibtype => \&map_bibtype,
    serial  => \&fix_serials,
    marc    => \&fix_marc,
);

