use strict;
use warnings;

sub make_field_map {

    # If this provides a string literal for the field, then the value
    # goes straight into that field. If it returns a sub, then that sub
    # must return a hash containing database fields and the value to go
    # in them. The fields returned will be considered extended values if
    # they don't already exist.
    {
        id                => [ "cardnumber" ],
        'contact address' => sub {
            build_address( 'primary', split( '\|', shift ) );
        },
        'alternative address' => sub {
            build_address( 'secondary', split( '\|', shift ) );
        },
        'telephone'  => 'phone',
        'occupation' => 'OCCUPATION',
        notes        => sub {
            my $val = shift;
            $val =~ s/\|/, /g;
            return () unless $val;
            return ( borrowernotes => $val );
        },
        'first name'      => 'firstname',
        'last name'       => 'surname',
        'fax number'      => 'fax', # polio
        'emailaddress'    => ['userid', 'email'],
        'borrower status' => [
            sub {
                my @val = split( '\|', shift );
                return () if empty_list(@val);
                return ( STATUS => \@val );
            },
            sub {
                return () unless grep { /Bad Borrower/ } @_;
                return (
                    __DEBARMENT => { comment => 'Bad Borrower' },
                    debarred    => '9999-12-31',
                );
              }
          ],
        'branch'   => 'IHCBRANCH',
        'region'   => 'REGION',
        'category' => sub {
            my @val = split( '\|', shift );
            return () if empty_list(@val);
            return ( CATEGORY => \@val );
        },
        'discoveredlibrary' => 'DISCOVERED',
        'child name'        => 'CHILDNAME',
        'child age'         => 'CHILDAGE',
        'futurecontact'     => 'CONTACT',
        'donations'         => 'DONATIONS',
        'badborrwernotes'  => sub { #sic
            my $val = shift;
            return () unless $val;
            $val =~ s/\|/, /g;
            return ( borrowernotes => $val );
        },
        'emailaddress2' => 'B_email',
        'literal:BORROWER' => 'categorycode',
        'literal:IHC' => 'branchcode',
        'literal:2114-01-01' => 'dateexpiry',
        chk => extract_date(
            'dateenrolled', '%d/%m/%Y %H:%M:%S',
            '%d-%b-%Y',     '%d-%b %Y',
            '%d/%m/%y',     '%d %b %Y'
        ),
        # changethispassword458
        'literal:$2a$08$X7hex3.aLejO/p028mum0.DF1nszRBzaRJqWQAk.dJw28cvdG5uta' => 'password',
    };
}

(
    fields     => make_field_map(),
    attributes => {
        map { $_ => 1 }
          qw( OCCUPATION STATUS IHCBRANCH REGION DISCOVERED CHILDNAME CHILDAGE CONTACT DONATIONS CATEGORY )
    },
);
