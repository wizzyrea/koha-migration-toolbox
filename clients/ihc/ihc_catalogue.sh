#!/bin/bash
perl -I ../../migration/Generic ../../migration/Generic/csvtomarc.pl \
    -i catalogue.csv \
    -o ihc.marc \
    -v -v \
    -f usmarc \
    -m 'func:extractyear:DATE OF PUB=marc:008/7-10?' \
    -m 'func:literal:t=marc:008/6' \
    -m 'func:split:|:AUTHOR=multimarc:100_a:700_a:...?' \
    -m 'PROD=marc:260_b?' \
    -m 'TITLE=special:title' \
    -m 'PUBLISHER=marc:260_b%?' \
    -m 'func:split:,:SOURCE=multimarc:490_a:490_v:...?' \
    -m 'PLACE OF PUB=marc:260_a?' \
    -m 'DATE OF PUB=marc:260_c?' \
    -m 'SERIES=marc:490_a%?' \
    -m 'func:cleanisbn:SBN=marc:020_a?' \
    -m 'EDITION=marc:250_a?' \
    -m 'func:split:|:KW=marc:650_a?' \
    -m 'func:ifmatch:anchor:ADVOCACY/HUMAN RIGHTS/INCLUSION:ADVOCACY:AGEING:AGEING:BEHAVIOUR MANAGEMENT:BEHAVIOURMGT:COMMUNICATION:COMMUNICATION:COMMUNITY LIVING:COMMLIVING:EDUCATION:EDUCATION:FAMILY/WHANAU & CHILDREN:FAMILY:FUNDRAISING:FUNDR:GOVERNMENT & LEGISLATION:GOVT:HEALTH & MEDICINE:HEALTHMED:HEALTH & SAFETY:HEALTHSAFE:HUMAN RELATIONSHIPS & SEXUALITY:RELATIONSHIPS:JUVENILE LITERATURE:JUVENILELIT:LEISURE:LEISURE:OTHER:OTHER:RESIDENTIAL/SUPPORTED LIVING:SUPPLIVING:SELF ADVOCACY:SELFADV:SERVICES:SERVICES:STAFF MANAGEMENT:STAFFMGT:SYNDROMES & CONDITIONS:SYNDROMES:TRANSITIONS:TRANSITIONS:VOCATIONAL/SUPPORTED:VOCATIONAL:Infomail head=marc:901_a?' \
    -m 'func:split:|:ABSTRACT=marc:520_a?' \
    -m 'func:split:|:NOTE=marc:500_a?' \
    -m 'PHYS DESC=marc:300_a?' \
    -m 'func:prefix:Copyright Permission: :Copyright Permission=marc:506_a?' \
    -m 'Table of contents=marc:505_a?' \
    -m 'func:coalesce:ACC No:Old ACC No=marc:998_a?' \
    -m 'ID=marc:998_b' \
    -m 'URL=marc:856_u?' \
    --dateformat '%d %B %Y' \
    --dateformat '%d %B %y' \
    --dateformat '%d-%b-%Y' \
    --dateformat '%d-%B-%Y' \
    --dateformat '%d/%m/%Y' \
    --dateformat '%d/%m/%y' \
    --dateformat '%d-%B -%Y' \
    --dateformat '%d- %B-%Y' \
    --dateformat '%d-%B %Y' \
    --dateformat '%d_%B-%Y' \
    --dateformat '%d%B %Y' \
    -m 'DATE CAT=special:date:952_d?' \
    -m 'COSTREPLACE=marc:952_v?' \
    -m 'CALL NO=marc:952_o?' \
    -m 'func:coalesce:Bar Code:ACC No:Old ACC No=marc:952_p?' \
    -m 'func:literal:IHC=marc:952_a' \
    -m 'func:literal:IHC=marc:952_b' \
    -m 'func:ifmatch:Banner:BAN:Book:B:CD-ROM:CD:DVD:DVD:Journal:ART:Kit:K:TYPE=marc:952_y' \
    -m 'func:ifmatch:Banner:BAN:Book:B:CD-ROM:CD:DVD:DVD:Journal:ART:Kit:K:TYPE=marc:942_c' \
    -m 'func:coalesce:ACC No:Old ACC No=marc:952_i?' \
    -m 'func:ifmatch:archive:*:Book Status=marc:952_x?' \
    -m 'func:ifmatch:archive:1:Book Status=marc:942_n?' \
    -m 'func:ifmatch:Journal:2:TYPE=marc:952_5' \
    -m 'func:split:|:BookRevSource=marc:902_a?' \





# Skipped:
# Old ACC No
# Borrower, Due, Waiting, HistoryReturn, Loan Date, Loan Period, IssueDate,
#  - process this with another script.
# Copies
# Infomail head
# ID
# TodaysDate - what stupidity is this?
# Te Puna
# Book status - convert to lost status or something?
# ResourceRatingBorrow - add to ratings system?
# ResourceFeedback - add to comments?
# IssuesNos, IssuesDates - always empty
# BookRevSource: I don't know what do do with this
# BookCovers: we can't use this directly
# StatusMissingDate


# Notes:
# Source: figure how to split this up better
# PHYS DESC: remove 300$a being required from the framework
# ACC No: 998$a
# ID: 998$b
# Table of contents: going in to 500$a, maybe should go into 505
