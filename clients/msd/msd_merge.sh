#!/bin/bash

../../migration/Generic/merge_marc.pl \
    --first records.marc \
    --second items.mrc \
    --matchfirst 020a \
    --filterfirst isbn \
    --matchsecond 001 \
    --mergeonly 952 \
    --output msd.marc
