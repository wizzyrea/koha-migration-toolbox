sub fix_ward {
    my ($attrib, $val) = @_;

    my %map = (
        Urban => 'U',
        Rural => 'R',
        'South Taranaki' => 'ST',
        'New Plymouth' => 'NP',
        InterLoan => 'I',
        Other => 'O',
    );

    $code = $map{$val};
    die "No ward code found to match $val" if !$code;
    return ($attrib, $code);
}

( attributes =>
      { department => curry( \&fix_ward, 'WARD' ), } );
